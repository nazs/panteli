$(function() {

	//make menus drop automatically
	$('ul.nav li.dropdown').hover(function() {
		$('.dropdown-menu', this).fadeIn();
	}, function() {
		$('.dropdown-menu', this).fadeOut('fast');
	});

	// show modals
	$('.modalphotos img').on('click', function() {
		$('#modal').modal({
			show: true,
		})

		var mysrc = this.src.substr(0, this.src.length-7) + '.jpg';
		$('#modalimage').attr('src', mysrc);
		var imgtag = this.alt;
		$('#imagetag').html(imgtag);
		$('#modalimage').on('click', function(){
			$('#modal').modal('hide')
		})
	});

	//$('#contact-email').val('hello');
	var email = $('#contact-email');
	var name = $('#contact-name');
	var msg = $('#contact-message');
	var sDate = $('#start2');
	var eDate = $('#end2');
	var errCount = []; //['hello', 'world', 'me'];
	
	
 	$("#contact-form").submit( function() {
 	//$("input[type=submit]").click( function() {
		//$(email).css(loginErr2).val('');

		var errCount = [];
		if(email.val() == '') {
			$('#emailReq').html('email required');
			errCount.push('email');
			//return false;
		}
		if(name.val() == '') {
			$('#nameReq').html('name required');
			errCount.push('name');
			//return false;
		}
		if(msg.val() == '') {
			$('#msgReq').html('message required');
			errCount.push('msg');
			//return false;
		}

		if(sDate.val() == '') {
			$('#startReq').html('Start Date required');
			errCount.push('start');
		}
		if(eDate.val() == '') {
			$('#endReq').html('End Date required');
			errCount.push('end');
		}

		if(errCount.length > 0) {
			return false;
			//$('#counter').html(errCount.length);
		}		
	});
	
	$('#contact-email').focus(function() {
		errCount.pop('email');
		$('#emailReq').html('');
	});	

	$('#contact-name').focus(function() {
		errCount.pop('name');
		$('#nameReq').html('');
	});

	$('#contact-message').focus(function() {
		errCount.pop('msg');
		$('#msgReq').html('');
	});

	$('#start2').focus(function() {
		errCount.pop('start');
		$('#startReq').html('');
	})
	$('#end2').focus(function() {
		errCount.pop('end');
		$('#endReq').html('');
	});

	// login prcess starts
	var uid = $('#uid');
	var pwd = $('#pwd');
	

	$('#admin-login').submit( function() {

		var errCount = [];
					
		if(uid.val() == '' || pwd.val() == '') {
			$('#login-err').html('<p>please enter login details</p>');
			errCount.push('email');
		}
		
		if(errCount.length > 0) {
			return false;
			//$('#counter').html(errCount.length);
		}		
	});

	uid.focus(function() {
		errCount.pop('email');
		$('#login-err').html('');
	});	
	pwd.focus(function() {
		errCount.pop('email');
		$('#login-err').html('');
	});	

	// login process ends

	$('#popups').on('shown.bs.modal', function () {
	 // alert('hi');
	});

	$('#add').click( function() {
		//alert('hi');
		$('#pophere').addClass('modal modal-dialog');
	});

	// used by Restaurant and Wines & Beers Add Item
	$('.trig-add-item').click( function() {
		var myId = $(this).attr('id').split('-');
		//alert(myId);
		$('input#group').val(myId[1]);
		//var act = $('input.group').val().split('-');
		$('.pophere').html('Add item to ' + myId[0]);
		//$('.pophere').html("Hello <b>world</b>!");
		$('.add-err').html('');
		$('textarea').removeClass('price_empty');
		$('#new-price').removeClass('price_empty').val('');
	});

	
	// add item for Restaurant and Wines
	$('#add-item').submit( function() {
		var errorCount = [];
		var item = $('#new-item').val();
		var price = $('#new-price').val();
		var place = $('.add-err');

		if(!item && !price) {
			errorCount.push('both fields');
			place.html('Both fields required.');
			$('textarea').addClass('price_empty');
			$('#new-price').addClass('price_empty');			

		} else if(!item) { 
			$('textarea').addClass('price_empty');
			errorCount.push('item');
			place.html('Item name required.');
			$('#new-price').addClass('price_empty');

		} else if(price) {
		    if (/^[0-9\.]*$/.test(price) == false) {
		    	place.html('only numeric for price');
		    	errorCount.push('price-invalid');
		    }

		} else {
			place.html('empty price');
			errorCount.push('price-empty');
		}

		if(errorCount.length > 0) {
			return false;			
		}
		
	});

	$('.allForms').submit( function() {
		var errorCount = [];

		$('input').each(function() {
			if($(this).val() == '' ) {
				errorCount.push('error');
			}		
		});
		$('textarea').each(function() {
			if($(this).val() == '' ) {
				errorCount.push('error');
			}		
		});

		if(errorCount.length > 0) {
			return false;			
		}		
	});

	$('input.price2').change( function() {
		if($(this).val() == '0.00') {
			//$(this).css('border', '1px solid red');
			alert('error');
			$(this).css({"border" : "1px solid red"})
		}
	})	
	
	// PASSWORD CHANGE
	var newPass = $('#newPass');
	var confirmPass = $('#confirmPass');
	var passErr = $('#pass-err');	

	$('#passChange').submit( function() {

		if(!(newPass).val() && !confirmPass.val()) {
			passErr.html('Please fill out both fields.');
			return false;
		} else if (newPass.val() != confirmPass.val() ) {
			passErr.html('Fields unmatched.');
			return false;
		}
	});

	newPass.focus( function() {
		passErr.html('');
	});
	confirmPass.focus( function() {
		passErr.html('');
	});

	
	// ADD MENU ITEMS
	var err = $('#frm-err');
	var newItem = $('#new-item');
	var price = $('#price');
	//for (var i=1; i <=6; i++) {

		//var currForm = '#add-item-' + i;

		$('.newnew').submit( function() {
			if(!newItem.val() && !price.val()) {
				err.html('Please fill out all fields.');
				return false;

			}
		});

	//}

	// ALL MENUS

	$('input[type=text]').focus( function() {
		if(!$(this).val()) {
			$(this).addClass('price_not_empty');
			if($(this).hasClass('price_empty')) {
				$(this).removeClass('price_empty');
			}
		}
	});	

	$('input[type=text]').blur( function() {
		if(!$(this).val()) {
			$(this).addClass('price_empty');
			if($(this).hasClass('price_not_empty')) {
				$(this).removeClass('price_not_empty');
			}
		}
	});

	$('textarea').blur( function() {
		if(!$(this).val()) {
			$(this).addClass('price_empty');
			if($(this).hasClass('price_not_empty')) {
				$(this).removeClass('price_not_empty');
			}
		}
	});

	$('textarea').focus( function() {
		if(!$(this).val()) {
			$(this).addClass('price_not_empty');
			if($(this).hasClass('price_empty')) {
				$(this).removeClass('price_empty');
			}
		}
	});


	$('form.ajax').submit( function() {

		console.log('trigger');
/*		var that = $(this),
			url = that.attr('action'),
			method = that.attr('method');
			data = {};

		that.find('[name]').each( function(index, value) {
			var that = $(this),
				name = that.attr('name');
				value = that.val();

			data[name] = value;
			//console.log(value);
		});

		console.log(data);*/

/*		$.ajax({
			url: url,
			type: type,
			data: data,
			success: function(response) {

			}
		})*/

		return false;
	});

});

