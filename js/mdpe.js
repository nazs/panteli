$(document).ready(function(){
	$("[class^='cartb']").css({'width': '50px', 'height': '20px' });
	$("[class^='price']").css({'width': '50px', 'height': '20px', 'border':'0px solid #000', 'background-color':'#fff' });
	/* $("[class^='cat']").css({'width': '50px', 'height': '20px' }); */
	$("[class^='err']").css("color", "#000").text(''); ////$("#error").css({'width':($("table #big").width()), 'border':'1px solid #000', 'text-align':'center', 'height':$(window).height()/3});
	
	//$("#grand").css();
	var subTotal = {'width': '90px', 'height': '20px', 'border':'0px solid #999','text-align':'right' };
	var formatGood = {'border':'1px solid #999', 'width':'53px', 'height': '21px'};
	var formatBad = {'border':'1px solid red', 'width':'53px'};
	var errors = {'color':'#D11919'};
	var loginErr = {'border':'1px solid red', 'background':'url(img/req.png) no-repeat 1px 4px',  'color':'red'};
	var loginErr2 = {'background':'#eee url(img/req.png) no-repeat 1px 30px',  'color':'#7A5C99', 'border':'1px solid #fff'};
	// CATALOG
	$("[id^='cat']").change(function() {
		var id = $(this).attr('id');//.split(",");
		id = id.split(",");
		//id = id[1];
		var errId = id[2];
		//alert(id[2]);		//var id = $(this).attr('id').replace('cat', ''); 	
		$("[id^='cat']").keyup(function() {
			$("input").css(formatGood);
			$(".err"+ errId).html(''); //.css({'font':'11px Arial', 'color': 'red','float':'right'});
		});
		var item = $(this);	
		var qty = $(this).val();
		var badMsg = 'Quantity, "<strong><font color=blue>'+qty+'</font></strong>" is invalid.';
		if(qty.match(/^\d+$/)) {
			if(qty > 0) {
				msg = ''; $(this).css(formatGood);		
			} else { 
				item.val('').css(formatBad); 
				setTimeout(function() { item.focus().css('border','1px solid #999'); }, 200);					
				msg = badMsg;   
			}
		} else {
			//item.css(formatBad); 
			item.val('').css(formatBad); 
			setTimeout(function() { 
				item.focus(); 
				/* item.blur(function() {
					item.css(formatGood)
				});	 */
			}, 200);	// //setTimeout(function() { item.css('border','1px solid #999'); }, 2000);		
			
			msg = badMsg; 
		}
		
		$(".err"+errId).html(msg).css({'font':'11px Arial', 'color': 'red','float':'right'});
		//setTimeout(function() { $(".err"+ id).html(''); }, 5000);	
	});		
	// CART check value
	$("[id^='qty']").change(function() {
		var id = $(this).attr('id');//.split(",");
		id = id.split(",");
		var errId = id[2];
		
		//var id = $(this).attr('class').replace('cartb', '');
		$("[id^='qty']").keyup(function() {
			$("[id^='qty']").css(formatGood);
			$(".err"+ errId).html(''); //.css({'font':'11px Arial', 'color': 'red','float':'right'});
		});
		var item = $(this);	
		var qty = $(this).val();
		var badMsg = 'Quantity, "<strong><font color=blue>'+qty+'</font></strong>" is invalid.';
		if(qty.match(/^\d+$/)) {
			if(qty >= 0) {
				msg = ''; $(this).css(formatGood);		
				$("[id^='qty']").css(formatGood);
			} else { 
				item.val('').css(formatBad); 
				setTimeout(function() { 
					item.focus();//.css('border','1px solid #999'); 
				}, 200);					
				msg = badMsg; 
				//item.val( $("#orig-qty"+id).val() );	
				//item.val(88);
			}
		} else {
			//item.css(formatBad); 
			item.val('').css(formatBad); 
			setTimeout(function() { 
				item.focus(); 
			}, 200);	// //setTimeout(function() { item.css('border','1px solid #999'); }, 2000);		
			
			msg = badMsg; 
			//item.val(99);	
			//item.val( $("#orig-qty"+id).val() );	
		}
		
		$(".err"+ errId).html(msg).css({'font':'11px Arial', 'color': 'red','float':'right'});
		//setTimeout(function() { $(".err"+ id).html(''); }, 5000);	
	});		

/////////////// CART calculate values
/* 	$("[id^='qty']").change(function() {
		
		var formatDisabled = {'border':'0px solid #000','background': '#fff', 'text-align': 'right'};
		var id = $(this).attr('id').replace('qty', '');
		var qty = parseInt($("#qty"+id).val()), carton = parseInt($("#carton"+id).text()), price = $("#price"+id).text(), grandTotal = $("#hiddenGrand").val(), hPrice = $("#hiddenPrice"+id).val();
		grandTotal = grandTotal.replace(',', '');
		price = price.replace('£', '');
		var newprice = qty * carton * price; //$("#price"+id).val() * $("#price"+id).val(); // qty * carton * price; //
		grandTotal = Number(grandTotal) - Number(hPrice) + Number(newprice);
		$("#cartc"+id).text(newprice).number(true,2);
		$("#grand").val(grandTotal).number(true,2);
		$("[class^='cartb']").css({'width': '50px', 'height': '20px' });
	});	 */		
	
////////////////////	
	$("form#prod").submit(function(e){	
		var sum = 0;
		$("input").each(function(){ 
		sum += Number($(this).val()); 
		}); 		
		//alert(sum);
		if(sum == 0) {
			msg = "Empty submission."
			e.preventDefault();
			//$("#error").html(msg);
			//alert(msg);
		//	$("#dialog-modal").dialog();

			$(function() {
				$( "#dialog-modal" ).dialog({
					height: 100, width: 100,
					modal: true
					});
				});
		}		
	});
	
	$("#tableTitle").html( $("#catTitle").html() ) ;
	
/* 	$("a#empty").click( function(){
		var answer = confirm("Empty Cart?")
		if (answer){
			window.location.href='index.php?p=cart&empty';
		}
		return false;  
	}); */
	
	
	var req = '-- Required --';
	// login and password reset
	var user = $("#user"), pwd = $("#pwd"), email = $("#email");//, msg = $("#login_msg"), msg2 = $("#reset_msg");
 	$("#login").submit( function() {
 	//$("input[type=submit]").click( function() {
		$(email).css(loginErr2).val('');
		if(user.val() == '' && pwd.val() == '' ) {
			var txt = 'username & password required';
			//pwd.type='Text';
			user.css(loginErr);
			pwd.css(loginErr);
			return false;
		} else if (pwd.val() == '') {
			pwd.css(loginErr);
			return false;
		}
	}); 
	
	user.focus(function() {
		user.css(loginErr2);
		pwd.css(loginErr2);
		if($(this).val() == req) {
			$(this).val('');
		};
	});
	pwd.focus(function() {
		if(user.val() == req) { user.val('');}
		user.css(loginErr2);
		pwd.css(loginErr2);
		
		/* if($(this).val() == req) {
			$(this).val('');
		}; */
	});
	
	email.focus(function() {
		email.css(loginErr2);
		if($(this).val() == req) {
			$(this).val('');
		};
	});

	/*
 	$("#reset").submit( function() {
		$(user).css(loginErr2).val('');
		$(pwd).css(loginErr2);
		if(email.val() == '' || email.val() == req)  {
			var txt2 = 'email required';
			$(email).css(loginErr).val(req);
			//alert('email required.');
			//$(msg2).text(txt2).css(errors);
			//$(msg).text('');
			return false;
		}
	}); 
	
	email.keyup( function() {
		pwd.css(loginErr2);
		user.css(loginErr2).val('');
	});  */
	
	user.keyup( function() {
		email.css(loginErr2).val('');
	});
	
	pwd.keyup( function() {
		email.css(loginErr2).val('');
	});
	
	//$("#sum").text( Number($("#cost1").text()) + Number($("#cost2").text()) );

	$("#yesno").click( function() {
		e.preventDefault();
		 $( "#dialog-confirm" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Proceed": function() {
					window.location.href='index.php?p=cart&empty';
					//$( this ).dialog( "close" );
				},
				"Cancel": function() {
					$( this ).dialog( "close" );
				}
			}
		});	
	});

	/* ADD DESCRIPTION */
	
	var desc = $("#description");
	var tfcode = $("#tf_code");
	var col = $("#colour");
	$("#edit").submit( function() {
		var abort = false;
		if(desc.val() == "") {
			desc.css({'border':'1px solid red','background':'url(img/req.png) #C2E0FF no-repeat'});
			abort = true;
		} // 679CC6  bg C2E0FF
	
		if(tfcode.val() == "") {
			tfcode.css({'border':'1px solid red','background':'url(img/req.png) #C2E0FF no-repeat'});
			abort = true;
		}
		
		if($("#colour option:selected").html() == "Select One") {
			col.css({'border':'1px solid red','background':'#FFCCFF'});
			abort = true;
		}
		// $("#selectId option:selected").html();
		//desc.after('<span>'+ $("#colour option:selected").html() +'</span>');
		
		//if(col.selectedIndex == 1) {$("#res").val("hello");}
		if (abort) { return false; } else { return true;}
			
	});
	
	desc.focus( function(){
		desc.css({'border':'1px solid #679CC6','background':'#C2E0FF'})
	});
	tfcode.focus( function(){
		tfcode.css({'border':'1px solid #679CC6','background':'#C2E0FF'})
	});
	//$("#login").submit( function() {
	col.focus( function(){
		col.css({'border':'1px solid #679CC6','background':'#C2E0FF'})
	});
});

// list cart


