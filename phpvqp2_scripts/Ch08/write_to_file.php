<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Handling Exceptions</title>
</head>
<body>
<?php # Script 8.1 - write_to_file.php

/*	This page attempts to write some data
 *	to a text file.
 *	Errors are handled using try...catch.
 */

// Identify the file:
$file = 'data.txt';

// Data to be written:
$data = "This is a line of data.\n";

// Start the try...catch block:
try {

	// Open the file:
	if (!$fp = fopen($file, 'w')) {
		throw new Exception('could not open the file.');
	}
	
	// Write to the file:
	if (!fwrite($fp, $data)) {
		throw new Exception('could not write to the file.');
	}

	// Close the file:
	if (!fclose($fp)) {
		throw new Exception('could not close the file.');
	}
	
	// If we got this far, everything worked!
	echo '<p>The data has been written.</p>';

} catch (Exception $e) {
	echo '<p>The process could not be completed because the script ' . $e->getMessage() . '</p>';
}

echo '<p>This is the end of the script.</p>';

?>
</body>
</html>
