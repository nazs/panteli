<?php # Script 7.8 - Triangle.php

/*	This page defines the Triangle class.
 *	The class contains two attributes: 
 *	- private $sides (array)
 *	- private $perimeter (number)
 *	The class contains three methods: 
 *	- __construct()
 *	- get_area()
 *	- get_perimeter()
 */
 
class Triangle extends Shape {

	// Declare the attributes:
	private $sides = array();
	private $perimeter = NULL;

	// Constructor:
	function __construct($s0 = 0, $s1 = 0, $s2 = 0) {
	
		// Store the values in the array:
		$this->sides[] = $s0;
		$this->sides[] = $s1;
		$this->sides[] = $s2;

		// Calculate the perimeter:
		$this->perimeter = array_sum($this->sides);
		
	} // End of constructor.
	
	// Method to calculate and return the area:
	public function get_area() {
	
		// Calculate and return the area:
		return (SQRT(
		($this->perimeter/2) *
		(($this->perimeter/2) - $this->sides[0]) * 
		(($this->perimeter/2) - $this->sides[1]) * 
		(($this->perimeter/2) - $this->sides[2])
		));
	
	} // End of get_area() method.
	
	// Method to return the perimeter:
	public function get_perimeter() {
		return $this->perimeter;
	} // End of get_perimeter() method.

} // End of Triangle class.

?>
