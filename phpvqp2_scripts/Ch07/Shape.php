<?php # Script 7.7 - Shape.php

/*	This page defines the Shape abstract class.
 *	The class contains no attributes.
 *	The class contains two abstract methods: 
 *	- get_area()
 *	- get_perimeter()
 */
 
abstract class Shape {

	// No attributes to declare.
	
	// No constructor or destructor defined here.

	// Method to calculate and return the area.
	abstract protected function get_area();
	
	// Method to calculate and return the perimeter.
	abstract protected function get_perimeter();
	
} // End of Shape class.

?>
