<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Triangle</title>
</head>
<body>
<?php # Script 7.9 - abstract.php

/*	This page uses the Triangle class (Script 7.8)
 *	which is derived from Shape (Script 7.7).
 */

// Define the __autoload() function:
function __autoload ($class) {
	require_once($class . '.php');
}

// Set the triangle's sides:
$side1 = 5;
$side2 = 12;
$side3 = 13;

// Print a little introduction:
echo "<h3>With sides of $side1, $side2, and $side3...</h3>";
	
// Create a new triangle:
$t = new Triangle($side1, $side2, $side3);

// Print the area.
echo '<p>The area of the triangle is ' . $t->get_area() . '</p>';
	
// Print the perimeter.
echo '<p>The perimeter of the triangle is ' . $t->get_perimeter() . '</p>';

// Delete the object:
unset($t);

?>
</body>
</html>
