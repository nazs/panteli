<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Square</title>
</head>
<body>
<?php # Script 7.2 - square.php

/*	This page declares and uses the Square class
 *	which is derived from Rectangle (Script 6.5).
 */

// Include the class definition:
require_once ('Rectangle.php');

// Create the Square class.
// The class only adds its own constructor.
class Square extends Rectangle {

	// Constructor takes one argument.
	// This value is assigned to the
	// Rectangle width and height attributes.
	function __construct($side = 0) {
		$this->width = $side;
		$this->height = $side;
	}
	
} // End of Square class.

// Rectangle dimensions:
$width = 21;
$height = 98;

// Print a little introduction:
echo "<h3>With a width of $width and a height of $height...</h3>";
	
// Create a new rectangle:
$r = new Rectangle($width, $height);

// Print the area.
echo '<p>The area of the rectangle is ' . $r->get_area() . '</p>';
	
// Print the perimeter.
echo '<p>The perimeter of the rectangle is ' . $r->get_perimeter() . '</p>';

// Square dimensions:
$side = 60;

// Print a little introduction:
echo "<h3>With each side being $side...</h3>";
	
// Create a new object:
$s = new Square($side);

// Print the area.
echo '<p>The area of the square is ' . $s->get_area() . '</p>';
	
// Print the perimeter.
echo '<p>The perimeter of the square is ' . $s->get_perimeter() . '</p>';

// Delete the objects:
unset($r, $s);

?>
</body>
</html>
