<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Pets</title>
</head>
<body>
<?php # Script 7.1 - pets1.php

/*	This page defines and uses
 *	the Pet, Cat, and Dog classes. 
 */

# ******************* #
# ***** CLASSES ***** #

/* Class Pet.
 *	The class contains one attribute: name.
 *	The class contains three methods: 
 *	- __construct()
 *	- eat()
 *	- go_to_sleep()
 */
class Pet {

	// Declare the attributes:
	public $name;

	// Constructor assigns the pet's name:
	function __construct($pet_name) {
		$this->name = $pet_name;
	}
	
	// Pets can eat:
	function eat() {
		echo "<p>$this->name is eating.</p>";
	}
	
	// Pets can sleep:
	function go_to_sleep() {
		echo "<p>$this->name is sleeping.</p>";
	}

} // End of Pet class.


/* Cat class extends Pet.
 * Cat has additional method: climb().
 */
class Cat extends Pet {
	function climb() {
		echo "<p>$this->name is climbing.</p>";
	}
} // End of Cat class.


/* Dog class extends Pet.
 * Dog has additional method: fetch().
 */
class Dog extends Pet {
	function fetch() {
		echo "<p>$this->name is fetching.</p>";
	}
} // End of Dog class.


# ***** END OF CLASSES ***** #
# ************************** #

// Create a dog:
$dog = new Dog('Satchel');

// Create a cat:
$cat = new Cat('Bucky');

// Feed them:
$dog->eat();
$cat->eat();

// Nap time:
$dog->go_to_sleep();
$cat->go_to_sleep();

// Do animal-specific thing:
$dog->fetch();
$cat->climb();

// Delete the objects:
unset($dog, $cat);

?>
</body>
</html>
