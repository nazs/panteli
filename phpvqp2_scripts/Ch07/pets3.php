<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Pets</title>
</head>
<body>
<?php # Script 7.5 - pets3.php

/*	This page defines and uses
 *	the Pet, Cat, and Dog classes. 
 */

# ******************* #
# ***** CLASSES ***** #

/* Class Pet.
 *	The class contains one attribute: name.
 *	The class contains four methods: 
 *	- __construct()
 *	- eat()
 *	- go_to_sleep()
 *	- play()
 */
class Pet {

	// Declare the attributes:
	public $name;

	// Constructor assigns the pet's name:
	function __construct($pet_name) {
		$this->name = $pet_name;
		self::go_to_sleep();
	}
	
	// Pets can eat:
	function eat() {
		echo "<p>$this->name is eating.</p>";
	}
	
	// Pets can sleep:
	function go_to_sleep() {
		echo "<p>$this->name is sleeping.</p>";
	}

	// Pets can play:
	function play() {
		echo "<p>$this->name is playing.</p>";
	}

} // End of Pet class.


/* Cat class extends Pet.
 * Cat overrides play().
 */
class Cat extends Pet {
	function play() {
		
		// Call the Pet::play() method:
		parent::play();
		
		echo "<p>$this->name is climbing.</p>";
		
	}
} // End of Cat class.


/* Dog class extends Pet.
 * Dog overrides play().
 */
class Dog extends Pet {
	function play() {
		
		// Call the Pet::play() method:
		parent::play();
		
		echo "<p>$this->name is fetching.</p>";
	}
} // End of Dog class.


# ***** END OF CLASSES ***** #
# ************************** #

// Create a dog:
$dog = new Dog('Satchel');

// Create a cat:
$cat = new Cat('Bucky');

// Create an unknown type of pet:
$pet = new Pet('Rob');

// Feed them:
$dog->eat();
$cat->eat();
$pet->eat();

// Nap time:
$dog->go_to_sleep();
$cat->go_to_sleep();
$pet->go_to_sleep();

// Have them play:
$dog->play();
$cat->play();
$pet->play();

// Delete the objects:
unset($dog, $cat, $pet);

?>
</body>
</html>
