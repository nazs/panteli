<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Pets</title>
</head>
<body>
<?php # Script 7.6 - static.php

/*	This page defines and uses
 *	the Pet, Cat, and Dog classes. 
 */

# ******************* #
# ***** CLASSES ***** #

/* Class Pet.
 *	The class contains two attributes: 
 *	- protected name
 *	- private static count
 *	The class contains three methods: 
 *	- __construct()
 *	- __destruct()
 *	- public static get_count()
 */
class Pet {

	// Declare the attributes:
	protected $name;
	
	private static $count = 0;

	// Constructor assigns the pet's name
	// and increments the counter.
	function __construct($pet_name) {
	
		$this->name = $pet_name;
		
		// Increment the counter:
		self::$count++;
		
	}
	
	// Destructor decrements the counter:
	function __destruct() {
		self::$count--;
	}

	// Static method for returning the counter:
	public static function get_count() {
		return self::$count;
	}

} // End of Pet class.


/* Cat class extends Pet. */
class Cat extends Pet {
} // End of Cat class.

/* Dog class extends Pet. */
class Dog extends Pet {
} // End of Dog class.

/* Ferret class extends Pet. */
class Ferret extends Pet {
} // End of Ferret class.

/* PygmyMarmoset class extends Pet. */
class PygmyMarmoset extends Pet {
} // End of PygmyMarmoset class.


# ***** END OF CLASSES ***** #
# ************************** #

// Create a dog:
$dog = new Dog('Old Yeller');

// Print the number of pets:
echo '<p>After creating a Dog, I now have ' . Pet::get_count() . ' pet(s).</p>';

// Create a cat:
$cat = new Cat('Bucky');
echo '<p>After creating a Cat, I now have ' . Pet::get_count() . ' pet(s).</p>';

// Create another pet:
$ferret = new Ferret('Fungo');
echo '<p>After creating a Ferret, I now have ' . Pet::get_count() . ' pet(s).</p>';

// Tragedy strikes!
unset($dog);
echo '<p>After tragedy strikes, I now have ' . Pet::get_count() . ' pet(s).</p>';

// Pygmy Marmosets are so cute:
$pygmymarmoset = new PygmyMarmoset('Toodles');
echo '<p>After creating a Pygmy Marmoset, I now have ' . Pet::get_count() . ' pet(s).</p>';

// Delete the objects:
unset($cat, $ferret, $pygmymarmoset);

?>
</body>
</html>
