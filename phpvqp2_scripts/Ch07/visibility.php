<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Visibility</title>
</head>
<body>
<?php # Script 7.4 - visibility.php

/*	This page defines and uses
 *	the Test and LittleTest classes. 
 */

# ******************* #
# ***** CLASSES ***** #

/* Class Test.
 *	The class contains three attributes:
 *	- public $public
 *	- protected $protected
 *	- private $private
 *	The class defines one method: print_var().
 */
class Test {

	// Declare the attributes:
	public $public = 'public';
	protected $protected = 'protected';
	private $private = 'private';	

	// Function for printing a variable's value:
	function print_var($var) {
		echo "<p>In Test, \$$var equals '{$this->$var}'.</p>";
	}

} // End of Test class.


/* LittleTest class extends Test.
 * LittleTest overrides print_var().
 */
class LittleTest extends Test {
	// Function for printing a variable's value:
	function print_var($var) {
		echo "<p>In LittleTest, \$$var equals '{$this->$var}'.</p>";
	}

} // End of LittleTest class.


# ***** END OF CLASSES ***** #
# ************************** #

// Create the objects:
$parent = new Test();
$child = new LittleTest();

// Print the current value of $public:
echo '<h2>Public</h2>';
echo '<h3>Initially...</h3>';
$parent->print_var('public');
$child->print_var('public');

// Modify $public and reprint:
echo '<h3>Modifying $parent->public...</h3>';
$parent->public = 'modified';
$parent->print_var('public');
$child->print_var('public');

// Print the current value of $protected:
echo '<hr /><h2>Protected</h2>';
echo '<h3>Initially...</h3>';
$parent->print_var('protected');
$child->print_var('protected');

// Attempt to modify $protected and reprint:
echo '<h3>Attempting to modify $parent->public...</h3>';
$parent->protected = 'modified';
$parent->print_var('protected');
$child->print_var('protected');

// Print the current value of $private:
echo '<hr /><h2>Private</h2>';
echo '<h3>Initially...</h3>';
$parent->print_var('private');
$child->print_var('private');

// Attempt to modify $private and reprint:
echo '<h3>Attempting to modify $parent->private...</h3>';
$parent->private = 'modified';
$parent->print_var('private');
$child->print_var('private');

// Delete the objects:
unset($parent, $child);

?>
</body>
</html>
