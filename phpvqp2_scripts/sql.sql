################
# Chapter 1:
################

# Database-driven arrays

USE test; 

CREATE TABLE tasks ( 
task_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
parent_id INT UNSIGNED NOT NULL DEFAULT 0, 
task VARCHAR(100) NOT NULL, 
date_added TIMESTAMP NOT NULL, 
date_completed TIMESTAMP, 
PRIMARY KEY (task_id), 
INDEX parent (parent_id), 
INDEX added (date_added), 
INDEX completed (date_completed) 
);

INSERT INTO tasks (task) VALUES ('Must Do This!'); 
SELECT * FROM tasks; 

################
# Chapter 3:
################

# Storing Sessions in a Database

USE test;

CREATE TABLE sessions ( 
id CHAR(32) NOT NULL, 
data TEXT, 
last_accessed TIMESTAMP NOT NULL, 
PRIMARY KEY (id) 
); 

# Working with U.S. Zip Codes

CREATE DATABASE zips; 

CREATE TABLE zip_codes ( 
zip_code INT(5) UNSIGNED ZEROFILL NOT NULL, 
latitude DOUBLE(9,6), 
longitude DOUBLE(9,6), 
city VARCHAR(60) NOT NULL, 
state CHAR(2) NOT NULL, 
county VARCHAR(60) NOT NULL, 
zip_class VARCHAR(12) NOT NULL, 
PRIMARY KEY (zip_code) 
); 

LOAD DATA INFILE '/tmp/ZIP_CODES.txt' 
INTO TABLE zip_codes 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n';

ALTER TABLE zip_codes DROP COLUMN zip_class; 

UPDATE zip_codes SET latitude=NULL, longitude=NULL WHERE latitude='';

CREATE TABLE stores ( 
store_id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT, 
name VARCHAR(60) NOT NULL, 
address1 VARCHAR(100) NOT NULL, 
address2 VARCHAR(100) default NULL, 
zip_code INT(5) UNSIGNED ZEROFILL NOT NULL, 
phone VARCHAR(15) NOT NULL, 
PRIMARY KEY (store_id), 
KEY (zip_code) 
); 

INSERT INTO stores (name, address1, address2, zip_code, phone) VALUES 
('Ray''s Shop', '49 Main Street', NULL, '63939', '(123) 456-7890'), 
('Little Lulu''s', '12904 Rockville Pike', '#310', '10580', '(123) 654- 7890'), 
('The Store Store', '8200 Leesburg Pike', NULL, '02461', '(123) 456- 8989'), 
('Smart Shop', '9 Commercial Way', NULL, '02141', '(123) 555-7890'), 
('Megastore', '34 Suburban View', NULL, '31066', '(555) 456-7890'), 
('Chain Chain Chain', '8th & Eastwood', NULL, '80726', '(123) 808-7890'), 
('Kiosk', 'St. Charles Towncenter', '3890 Crain Highway', '63384', '(123) 888-4444'), 
('Another Place', '1600 Pennsylvania Avenue', NULL, '05491', '(111) 456- 7890'), 
('Fishmonger''s Heaven', 'Pier 9', NULL, '53571', '(123) 000-7890'), 
('Hoo New', '576b Little River Turnpike', NULL, '08098', '(123) 456-0000'), 
('Vamps ''R'' Us', 'Our Location', 'Atwood Mall', '02062', '(222) 456- 7890'), 
('Five and Dime', '9 Constitution Avenue', NULL, '73503', '(123) 446- 7890'), 
('A & P', '890 North Broadway', NULL, '85329', '(123) 456-2323'), 
('Spend Money Here', '1209 Columbia Pike', NULL, '10583', '(321) 456- 7890');

# Creating Stored Functions

CREATE FUNCTION zips.return_ distance (lat_a DOUBLE, long_ a DOUBLE, 
lat_b DOUBLE, long_b DOUBLE) RETURNS DOUBLE 
BEGIN 
DECLARE distance DOUBLE; 
SET distance = SIN(RADIANS(lat_a)) * SIN(RADIANS(lat_b)) 
+ COS(RADIANS(lat_a)) 
* COS(RADIANS(lat_b)) 
* COS(RADIANS(long_a - long_b)); 
RETURN((DEGREES(ACOS(distance))) * 69.09); 
END 

################
# Chapter 4:
################

# Authentication with Pear Auth

CREATE TABLE auth ( 
username VARCHAR(50) NOT NULL, 
password VARCHAR(32) NOT NULL, 
PRIMARY KEY (username), 
KEY (password);


CREATE TABLE users ( 
user_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
email VARCHAR(60) NOT NULL, 
pass CHAR(40) NOT NULL, 
first_name VARCHAR (20) NOT NULL, 
last_name VARCHAR(40) NOT NULL, 
PRIMARY KEY (user_id), 
UNIQUE (email), 
KEY (email, pass) 
);

################
# Chapter 5:
################

# Creating the Database

CREATE DATABASE ecommerce; 

USE ecommerce; 

CREATE TABLE customers ( 
customer_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
email VARCHAR(40) NOT NULL, 
pass CHAR(40) NOT NULL, 
first_name VARCHAR(20) NOT NULL, 
last_name VARCHAR(30) NOT NULL, 
address1 VARCHAR(60) NOT NULL, 
address2 VARCHAR(60), 
city VARCHAR(30) NOT NULL, 
state CHAR(2) NOT NULL, 
zip_code VARCHAR(10) NOT NULL, 
phone VARCHAR(15), 
PRIMARY KEY (customer_id), 
UNIQUE (email), 
KEY email_pass (email, pass) 
) ENGINE=MyISAM;

CREATE TABLE orders ( 
order_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
customer_id INT(5) UNSIGNED NOT NULL, 
total DECIMAL(10,2) NOT NULL, 
order_date TIMESTAMP, 
PRIMARY KEY (order_id), 
KEY customer_id (customer_id), 
KEY order_date (order_date) 
) ENGINE=InnoDB; 

CREATE TABLE order_contents ( 
oc_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
order_id INT UNSIGNED NOT NULL, 
sw_id INT UNSIGNED NOT NULL, 
quantity TINYINT UNSIGNED NOT NULL DEFAULT 1, 
price DECIMAL(6,2) NOT NULL, 
ship_date DATETIME default NULL, 
PRIMARY KEY (oc_id), 
KEY order_id (order_id), 
KEY sw_id (sw_id), 
KEY ship_date (ship_date) 
) ENGINE=InnoDB; 

CREATE TABLE categories ( 
category_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
category VARCHAR(30) NOT NULL, 
description TEXT, 
PRIMARY KEY (category_id) 
) ENGINE=MyISAM; 

CREATE TABLE colors ( 
color_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
color VARCHAR(10) NOT NULL, 
PRIMARY KEY (color_id) 
) ENGINE=MyISAM; 

CREATE TABLE sizes ( 
size_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT, 
size VARCHAR(10) NOT NULL, 
PRIMARY KEY (size_id) 
) ENGINE=MyISAM; 

CREATE TABLE general_widgets ( 
gw_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT, 
category_id TINYINT UNSIGNED NOT NULL, 
name VARCHAR(30) NOT NULL, 
default_price DECIMAL(6,2) NOT NULL, 
description TEXT, 
PRIMARY KEY (gw_id), 
UNIQUE (name), 
KEY (category_id) 
) ENGINE=MyISAM; 

CREATE TABLE specific_widgets ( 
sw_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
gw_id MEDIUMINT UNSIGNED NOT NULL, 
color_id TINYINT UNSIGNED NOT NULL, 
size_id TINYINT UNSIGNED NOT NULL, 
price DECIMAL(6,2), 
in_stock CHAR(1), 
PRIMARY KEY (sw_id), 
UNIQUE combo (gw_id, color_id, size_id), 
KEY (gw_id), 
KEY (color_id), 
KEY (size_id) 
) ENGINE=MyISAM;

INSERT INTO categories (category) VALUES 
('Widgets That Wiggle'), 
('Widgets That Bounce'), 
('Widgets That Sit There'), 
('Non-widget Widgets'), 
('Fuzzy Widgets'), 
('Razor-sharp Widgets'); 

INSERT INTO colors (color) VALUES 
('Red'), 
('Blue'), 
('Heather'), 
('Stone'), 
('Dirt Brown'), 
('Mud Brown'); 

INSERT INTO sizes (size) VALUES 
('Wee'), 
('Little'), 
('Huge'), 
('Vast'), 
('Medium'), 
('Venti'); 

INSERT INTO general_widgets (category_id, name, default_price, description) VALUES 
(1, 'Wiggle Widget 1', 234.45, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(1, 'Wiggle Widget 2', 200.99, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(1, 'Wiggle Widget 3', 164.00, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(2, 'Bouncy Widget 1', 1.16, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(2, 'Bouncy Widget 2', 32.20, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(3, 'Useless Widget', 985.00, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(6, 'Barbed-wire Widget', 141.66, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(6, 'Rusty Nails Widget', 45.25, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '), 
(6, 'Broken Glass Widget', 8.00, 'This is the description of this widget. This is the description of this widget. This is the description of this widget. This is the description of this widget. '); 

INSERT INTO specific_widgets (gw_id, color_id, size_id, price, in_stock) VALUES 
(1, 1, 2, NULL, 'Y'), 
(1, 1, 3, NULL, 'Y'), 
(1, 1, 4, NULL, 'Y'), 
(1, 3, 1, NULL, 'Y'), 
(1, 3, 2, NULL, 'Y'), 
(1, 4, 1, NULL, 'Y'), 
(1, 4, 2, NULL, 'N'), 
(1, 4, 3, NULL, 'N'), 
(1, 4, 6, NULL, 'Y'), 
(2, 1, 1, NULL, 'Y'), 
(2, 1, 2, NULL, 'Y'), 
(2, 1, 6, NULL, 'N'), 
(2, 4, 4, NULL, 'Y'), 
(2, 4, 5, NULL, 'Y'), 
(2, 6, 1, NULL, 'N'), 
(2, 6, 2, NULL, 'Y'), 
(2, 6, 3, NULL, 'Y'), 
(2, 6, 6, NULL, 'Y'), 
(3, 1, 1, 123.45, 'N'), 
(3, 1, 2, NULL, 'Y'), 
(3, 1, 6, 846.45, 'Y'), 
(3, 1, 4, NULL, 'Y'), 
(3, 4, 4, NULL, 'Y'), 
(3, 4, 5, 147.00, 'Y'), 
(3, 6, 1, 196.50, 'Y'), 
(3, 6, 2, 202.54, 'Y'), 
(3, 6, 3, NULL, 'N'), 
(3, 6, 6, NULL, 'Y'), 
(4, 2, 5, NULL, 'Y'), 
(4, 2, 6, NULL, 'Y'), 
(4, 3, 2, NULL, 'N'), 
(4, 3, 3, NULL, 'Y'), 
(4, 3, 6, NULL, 'Y'), 
(4, 5, 4, NULL, 'Y'), 
(4, 5, 6, NULL, 'N'), 
(4, 6, 2, NULL, 'Y'), 
(4, 6, 3, NULL, 'Y'); 


################
# Chapter 13:
################

USE test; 

CREATE TABLE users ( 
user_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
username VARCHAR(20) NOT NULL, 
userpass CHAR(40) NOT NULL, 
first_name VARCHAR(20) NOT NULL, 
last_name VARCHAR(40) NOT NULL, 
email VARCHAR(60) NOT NULL, 
PRIMARY KEY (user_id), 
UNIQUE (username) 
); 

INSERT INTO users (username, userpass, first_name, last_name, email) VALUES 
('sherif', SHA('deadwood1'), 'Seth', 'Bullock', 'seth@address.com'), 
('Al', SHA('deadwood2'), 'Al', 'Swearengen', 'al@thegem.org'), 
('Garret', SHA('deadwood3'), 'Alma', 'Garret', 'agarret@address.net'), 
('starman', SHA('deadwood4'), 'Sol', 'Star', 'solstar@bank.com');
