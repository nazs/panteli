<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Benchmarking Functions</title>
</head>
<body>
<?php # Script 12.2 - iterate.php

/*	This page performs benchmarks on a function. 
 *	This page requires the PEAR Benchmark package.
 */

// Function to be tested:
function dummy() {
	$data = 'This is some text.';
	echo '<!--<h1>echo() with single quotes</h1>
	<table border="0" width="90%" cellspacing="3" cellpadding="3" align="center">
		<tr>
			<td>' . $data . '</td>
			<td>' . $data . '</td>
			<td>' . $data . '</td>
		</tr>
		<tr>
			<td>' . $data . '</td>
			<td>' . $data . '</td>
			<td>' . $data . '</td>
		</tr>
	</table>
	<p>End of echo() with single quotes.</p>-->';
}

// Include the Iterate class definition:
require ('Benchmark/Iterate.php');

// Create and start an iteration:
$b = new Benchmark_Iterate();
$b->run(100, 'dummy');

// Get the results:
$r = $b->get();

// Print the results:
echo "<h1>Results: {$r['iterations']} iterations of the dummy() function took an average of {$r['mean']} seconds.</h1>";

// Delete the object:
unset($b);
?>
</body>
</html>
