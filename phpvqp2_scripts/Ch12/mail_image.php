<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Sending HTML Email with an Image</title>
</head>
<body>
<?php # Script 12.7 - mail_image.php

/*	This page sends an HTML email.
 *	This page requires the PEAR Mail and Mail_Mime packages.
 *	The email now contains an image.
 */
 
// Include the class definitions:
require_once ('Mail.php');
require_once ('Mail/mime.php');

// Define the data to use in the email body:
$text = 'Testing HTML Email with an Image
----------
Just some simple HTML.
You are not going to see the logo.';

$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Testing HTML Email with an Image</title>
</head>
<body>
<h1>Testing HTML Email with an Image</h1>
<hr />
<p>Just some <em>simple</em> HTML.</p>
<img src="logo2.png" alt="my logo" />
<p>That is what my logo would look like randomly placed in an email.</p>
</body>
</html>';

// Create the Mail_Mime object:
$mime = new Mail_Mime();

// Add the image:
$mime->addHTMLImage('logo2.png');

// Set the email body:
$mime->setTXTBody($text);
$mime->setHTMLBody($html);

// Set the headers:
$mime->setFrom('me@address.com');
$mime->setSubject('Testing HTML Email with an Image');

// Get the formatted code:
$body = $mime->get();
$headers = $mime->headers();

// Invoke the Mail class' factory() method:
$mail =& Mail::factory('mail');

// Send the email.
$mail->send('you@address.com', $headers, $body);

// Delete the objects:
unset($mime, $mail);

// Print a message, if you want.
echo '<p>The mail has been sent (hopefully).</p>';
?>
</body>
</html>
