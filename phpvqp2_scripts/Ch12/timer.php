<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Benchmarking Code</title>
</head>
<body>
<?php # Script 12.1 - timer.php

/*	This page performs benchmarks on three 
 *	different types of echo() statements.
 *	This page requires the PEAR Benchmark package.
 */

// Some dummy data to be printed:
$data = 'This is some text.';

// Include the Timer class definition:
require ('Benchmark/Timer.php');

// Create and start a timer:
$timer = new Benchmark_Timer();
$timer->start();

// Time a single-quote example:
$timer->setMarker('echo1');
echo '<h1>echo() with single quotes</h1>
<table border="0" width="90%" cellspacing="3" cellpadding="3" align="center">
	<tr>
		<td>' . $data . '</td>
		<td>' . $data . '</td>
		<td>' . $data . '</td>
	</tr>
	<tr>
		<td>' . $data . '</td>
		<td>' . $data . '</td>
		<td>' . $data . '</td>
	</tr>
</table>
<p>End of echo() with single quotes.</p>
';

// Time a double-quote example:
$timer->setMarker('echo2');
echo "<h1>echo() with double quotes</h1>
<table border=\"0\" width=\"90%\" cellspacing=\"3\" cellpadding=\"3\" align=\"center\">
	<tr>
		<td>$data</td>
		<td>$data</td>
		<td>$data</td>
	</tr>
	<tr>
		<td>$data</td>
		<td>$data</td>
		<td>$data</td>
	</tr>
</table>
<p>End of echo() with double quotes.</p>
";

// Time a heredoc example:
$timer->setMarker('heredoc');
echo <<<EOT
<h1>heredoc Syntax</h1>
<table border="0" width="90%" cellspacing="3" cellpadding="3" align="center">
	<tr>
		<td>$data</td>
		<td>$data</td>
		<td>$data</td>
	</tr>
	<tr>
		<td>$data</td>
		<td>$data</td>
		<td>$data</td>
	</tr>
</table>
<p>End of heredoc syntax.</p>
EOT;

// Set a final marker and stop the timer:
$timer->setMarker('end');
$timer->stop();

// Print the results:
echo '<hr /><h1>Results:</h1>';

echo '<p>Time required for the single quote echo(): ' . $timer->timeElapsed('echo1', 'echo2') . '</p>';

echo '<p>Time required for the double quote echo(): ' . $timer->timeElapsed('echo2', 'heredoc') . '</p>';

echo '<p>Time required for the heredoc echo(): ' . $timer->timeElapsed('heredoc', 'end') . '</p>';

// Delete the object:
unset($timer);
?>
</body>
</html>
