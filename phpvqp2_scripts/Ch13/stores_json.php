<?php # Script 13.5 - stores_json.php

/*	This page queries a database, returning
 *	a list of 10 stores and how far way
 *	they are from the submitted zip code.
 *	The page will be called by JavaScript.
 *	No HTML is required by this script!
 */
 
$zip = FALSE; // Flag variable.

// Validate that the page received $_GET['zip']:
if ( isset($_GET['zip']) &&
	( (strlen($_GET['zip']) == 5) || (strlen($_GET['zip']) == 10) )
	) {
	
	// Chop off the last four digits, if necessary.
	if (strlen($_GET['zip']) == 10) {
		$zip = substr($_GET['zip'], 0, 5);
	} else {
		$zip = $_GET['zip'];
	}
	
	// Make sure it's numeric:
	if (is_numeric($zip)) {

		// Connect to the database:
		$dbc = @mysqli_connect ('localhost', 'username', 'password', 'zips') OR die ('null');
		
		// Get the origination latitude and longitude:
		$q = "SELECT latitude, longitude FROM zip_codes WHERE zip_code='$zip'";
		$r = mysqli_query($dbc, $q);
		
		// Retrieve the results:
		if (mysqli_num_rows($r) == 1) {
		
			list($lat, $long) = mysqli_fetch_array($r, MYSQLI_NUM);

		} else { // Invalid zip.
			$zip = FALSE;
			mysqli_close($dbc);
		}

	} else { // Invalid zip.
		$zip = FALSE;
	}
	
}

if ($zip) { // Get the stores and distances.

	// Big, important query:
	$q = "SELECT name, CONCAT_WS('<br />', address1, address2), city, state, s.zip_code, phone, round(return_distance($lat, $long, latitude, longitude)) AS dist FROM stores AS s LEFT JOIN zip_codes AS z USING (zip_code) ORDER BY dist ASC LIMIT 5";
	$r = mysqli_query($dbc, $q);
	
	if (mysqli_num_rows($r) > 0) {
	
		// Initialize an array:
		$json = array();
		
		// Put each store into the array:
		while (list($name, $address	, $city, $state, $zip, $phone, $distance) = mysqli_fetch_array($r, MYSQLI_NUM)) {
		
			$json[] = array('name' => $name,
			'address' => $address, 
			'city' => ucfirst(strtolower($city)), 
			'state' => $state, 
			'zip' => $zip, 
			'phone' => $phone, 
			'distance' => $distance);
				
		}
		
		// Send the JSON data:
		echo json_encode($json) . "\n";
		
	} else { // No records returned.
		echo 'null';
	}

	mysqli_close($dbc);
	
} else { // Invalid zip.
	echo 'null';
}
?>

