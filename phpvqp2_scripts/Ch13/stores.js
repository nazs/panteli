// Script 13.6 - stores.js

/*	This page does all the magic for applying
 *	Ajax principles to a store retrieval form.
 *	The users's zip code is sent to a PHP 
 *	script which will return data in JSON format.
 */

// Function that starts the Ajax process:
function get_stores(zip) {

	if (ajax) { 

		// Call the PHP script.
		// Use the GET method.
		// Pass the zip code in the URL.
		ajax.open('get', 'stores_json.php?zip=' + encodeURIComponent(zip));

		// Function that handles the response:
		ajax.onreadystatechange = handle_stores;
		
		// Send the request:
		ajax.send(null);

		return false;
		
	} else { // Can't use Ajax!
		return true;
	}
	
} // End of get_stores() function.

// Function that handles the response from the PHP script:
function handle_stores() {
	
	// If everything's OK:
	if ( (ajax.readyState == 4) && (ajax.status == 200) ) {
	
		// Check the length of the response:
		if (ajax.responseText.length > 10) {
		
			// Send the response, in object form, 
			// to the show_stores() function:
			show_stores(eval('(' + ajax.responseText + ')'));	
			
		} else {
			document.getElementById('list').innerHTML = '<p>No stores matched your search.</p>';
		}
		
	}
	
} // End of handle_stores() function.

// Function that shows the list of stores:
function show_stores(stores) {

	// Initialize a string:
	var message = '';

	// Get each store:
	for (var i = 0 ; i < stores.length ; i++) {

		// Add to the string:
		message += '<h2>' + stores[i].name + '</h2>'
		+ '<p>' +  stores[i].address + '<br />'
		+ stores[i].city + ', ' + stores[i].state + ' '
		+ stores[i].zip + '<br />'
		+ stores[i].phone + '<br />(approximately ' 
		+ stores[i].distance + ' miles)</p>';

	}
	
	// Place the string in the page:
	document.getElementById('list').innerHTML = message;

} // End of show_stores() function.
