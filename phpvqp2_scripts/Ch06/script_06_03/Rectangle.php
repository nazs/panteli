<?php # Script 6.3 - Rectangle.php

/*	This page defines the Rectangle class.
 *	The class contains two attributes: width and height.
 *	The class contains four methods: 
 *	- set_size()
 *	- get_area()
 *	- get_perimeter()
 *	- is_square()
 */
 
class Rectangle {

	// Declare the attributes:
	public $width = 0;
	public $height = 0;
	
	// Method to set the dimensions.
	function set_size($w = 0, $h = 0) {
		$this->width = $w;
		$this->height = $h;
	}
	
	// Method to calculate and return the area.
	function get_area() {
		return ($this->width * $this->height);
	}
	
	// Method to calculate and return the perimeter.
	function get_perimeter() {
		return ( ($this->width + $this->height) * 2 );
	}
	
	// Method to determine if the rectange 
	// is also a square.
	function is_square() {
	
		if ($this->width == $this->height) {
			return true; // Square
		} else {
			return false; // Not a square
		}
		
	}

} // End of Rectangle class.

?>
