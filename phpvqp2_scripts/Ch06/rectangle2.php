<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Rectangle</title>
</head>
<body>
<?php # Script 6.6 - rectangle2.php

/*	This page uses the revised Rectangle class.
 *	This page shows a bunch of information
 *	about a rectangle.
 */

// Include the class definition:
require_once ('Rectangle.php');

// Define the necessary variables:
$width = 160;
$height = 75;

// Print a little introduction:
echo "<h3>With a width of $width and a height of $height...</h3>";
	
// Create a new object:
$r = new Rectangle($width, $height);

// Print the area.
echo '<p>The area of the rectangle is ' . $r->get_area() . '</p>';
	
// Print the perimeter.
echo '<p>The perimeter of the rectangle is ' . $r->get_perimeter() . '</p>';

// Is this a square?
echo '<p>This rectangle is ';
if ($r->is_square()) {
	echo 'also';
} else {
	echo 'not';
}
echo ' a square.</p>';

// Delete the object:
unset($r);

?>
</body>
</html>
