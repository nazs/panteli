<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Constructors and Destructors</title>
</head>
<body>
<?php # Script 6.7 - demo.php

/*	This page defines a Demo class
 *	and a demo() function.
 *	Both are used to show when
 *	constructors and destructors are called.
 */

// Define the class:
class Demo {

	// No attributes.
	
	// Constructor:
	function __construct() {
		echo '<p>In the constructor.</p>';
	}

	// Destructor:
	function __destruct() {
		echo '<p>In the destructor.</p>';
	}
	
} // End of Demo class.

// Define a demo() function:
function demo () {

	echo '<p>In the function. Creating a new object...</p>';
	$f = new Demo();
	echo '<p>About to leave the function.</p>';
	
}

// Create the object:
echo '<p>Creating a new object...</p>';
$o = new Demo();

// Call the demo function:
echo '<p>Calling the function...</p>';
demo();

// Delete the object:
echo '<p>About to delete the object...</p>';
unset($o);

echo '<p>End of the script.</p>';
?>
</body>
</html>
