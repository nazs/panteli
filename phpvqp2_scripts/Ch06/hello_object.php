<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Hello, world!</title>
</head>
<body>
<?php # Script 6.2 - hello_object.php

/*	This page uses the HelloWorld class.
 *	This page just says "Hello, world!".
 */

// Include the class definition:
require_once ('HelloWorld.php');

// Create the object:
$obj = new HelloWorld();

// Call the say_hello() method:
$obj->say_hello();

// Say hello in different languages:
$obj->say_hello('Italian');
$obj->say_hello('Dutch');
$obj->say_hello('French');

// Delete the object:
unset($obj);

?>
</body>
</html>
