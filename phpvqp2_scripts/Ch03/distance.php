<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Distance Calculator</title>
	<style type="text/css" title="text/css" media="all">
.error {
	color: #F30;
}
h3 {
	color: #00F;
}
</style>
</head>
<body>
<?php # Script 3.3 - distance.php

/*	This page uses the zips database to
 *	calculate the distance between a given
 *	point and some stores.
 *	The three closest stores are returned.
 */
 
$zip = 64154; //User's zip code.

// Print a caption:
echo "<h2>Nearest stores to $zip:</h2>\n";

// Connect to the database:
$dbc = @mysqli_connect ('localhost', 'username', 'password', 'zips') OR die ('<p class="error">Cannot connect to the database.</p></body></html>');

// Get the origination latitude and longitude:
$q = "SELECT latitude, longitude FROM zip_codes WHERE zip_code='$zip' AND latitude IS NOT NULL";
$r = mysqli_query($dbc, $q);

// Retrieve the results:
if (mysqli_num_rows($r) == 1) {

	list($lat, $long) = mysqli_fetch_array($r, MYSQLI_NUM);
	
	// Big, main, complex, wordy query:
	$q = "SELECT name, CONCAT_WS('<br />', address1, address2), city, state, stores.zip_code, phone, ROUND(DEGREES(ACOS(SIN(RADIANS($lat)) 
* SIN(RADIANS(latitude)) 
+ COS(RADIANS($lat))  
* COS(RADIANS(latitude))
* COS(RADIANS($long - longitude)))) * 69.09) AS distance FROM stores LEFT JOIN zip_codes USING (zip_code) ORDER BY distance ASC LIMIT 3";
	$r = mysqli_query($dbc, $q);
	
	if (mysqli_num_rows($r) > 0) {
	
		// Display the stores:
		while ($row = mysqli_fetch_array($r, MYSQLI_NUM)) {
			echo "<h3>$row[0]</h3>
<p>$row[1]<br />" . ucfirst(strtolower($row[2])) . ", $row[3] $row[4]<br />
$row[5] <br />
(approximately $row[6] miles)</p>\n";

		} // End of WHILE loop.
		
	} else { // No stores returned.

		echo '<p class="error">No stores matched the search.</p>';

	}
	
} else { // Invalid zip code.

	echo '<p class="error">An invalid zip code was entered.</p>';

}

// Close the connection:
mysqli_close($dbc);

?>
</body>
</html>
