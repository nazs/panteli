<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>XML Expat Parser</title>
	<style type="text/css" title="text/css" media="all">
.tag {
	color: #00F;
}
.content {
	color: #C03;
}
.attribute {
	color: #063;
}
.avalue {
	color: #000;
}
</style>
</head>
<body>
<pre>
<?php # Script 14.7 - expat.php

/*	This script will parse an XML file.
 *	It uses the Expat library, an event-based parser.
 */

// Define some constants to represent 
// the greater-than and less-than symbols.
define ('LT', '<span class="tag">&lt;');
define ('GT', '&gt;</span>');

// Function for handling the open tag:
function handle_open_element ($p, $element, $attributes) {

	// Make the element lowercase:
	$element = strtolower($element);
	
	// Do different things based upon the element:
	switch ($element) {
	
		case 'collection':
			echo LT . $element . GT . "\n";
			break;
		
		case 'book': // Indent books two spaces:
			echo '  ' . LT . $element . GT . "\n";
			break;
			
		case 'chapter': // Indent four spaces:
			echo '    ' . LT . $element;
			
			// Add each attribute:
			foreach ($attributes as $key => $value) {
				echo ' <span class="attribute">' . strtolower($key) . '="<span class="avalue">' . $value . '</span>"</span>';
			}
			echo GT;
			break;
		
		case 'cover': // Show the image.
		
			// Get the image info:
			$image = @getimagesize ($attributes['FILENAME']);
			
			// Make the image HTML:
			echo "<img src=\"{$attributes['FILENAME']}\" $image[3] border=\"0\" /><br />\n";
			break;
			
		// Indent everything else four spaces:
		default:
			echo '    ' . LT . $element . GT;
			break;
			
	} // End of switch.
	
} // End of handle_open_element() function.

// Function for handling the closing tag:
function handle_close_element ($p, $element) {

	// Make the element lowercase:
	$element = strtolower($element);
	
	// Indent closing book tags 2 spaces,
	// Do nothing with cover,
	// Do nothing special with everything else.
	if ($element == 'book') {
		echo '  ' . LT . '/' . $element . GT . "\n";
	} elseif ($element != 'cover') {
		echo LT . '/' . $element . GT . "\n";
	}
	
} // End of handle_close_element() function.

// Function for printing the content:
function handle_character_data ($p, $cdata) {
	echo "<span class=\"content\">$cdata</span>";
}

# ---------------------
# End of the functions.
# ---------------------

// Create the parser:
$p = xml_parser_create();

// Set the handling functions:
xml_set_element_handler ($p, 'handle_open_element', 'handle_close_element');
xml_set_character_data_handler ($p, 'handle_character_data');

// Read the file:
$file = 'books3.xml';
$fp = @fopen ($file, 'r') or die ("Could not open a file called '$file'.\n</body>\n</html>\n");
while ($data = fread ($fp, filesize($file))) {
	xml_parse ($p, $data, feof($fp));
}

// Free up the parser:
xml_parser_free($p);
?>
</pre>
</body>
</html>
