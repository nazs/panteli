<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>SimpleXML Parser</title>
</head>
<body>
<?php # Script 14.8 - simplexml.php

/*	This script will parse an XML file.
 *	It uses the simpleXML library, a DOM parser.
 */
 
 // Read the file:
$xml = simplexml_load_file('books3.xml');

// Iterate through each book:
foreach ($xml->book as $book) {

	// Print the title:
	echo "<h2>$book->title";
	
	// Check for an edition:
	if (isset($book->title['edition'])) {
		echo " (Edition {$book->title['edition']})";
	}
	
	echo "</h2><p>\n";
	
	// Print the author(s):
	foreach ($book->author as $author) {
	
		echo "Author: $author<br />\n";
		
	}
	
	// Print the other book info:
	echo "Published: $book->year<br />\n";
	
	if (isset($book->pages)) {
		echo "$book->pages Pages<br />\n";
	} 
	
	// Print each chapter:
	if (isset($book->chapter)) {
		echo '<ul>';
		foreach ($book->chapter as $chapter) {
		
			echo '<li>';
			
			if (isset($chapter['number'])) {
				echo "Chapter {$chapter['number']}: \n";
			}
			
			echo $chapter;
			
			if (isset($chapter['pages'])) {
				echo " ({$chapter['pages']} Pages)\n";
			}
			
			echo '</li>';
			
		}
		echo '</ul>';
	}
	
	// Handle the cover:
	if (isset($book->cover)) {
	
		// Get the image info:
		$image = @getimagesize ($book->cover['filename']);
		
		// Make the image HTML:
		echo "<img src=\"{$book->cover['filename']}\" $image[3] border=\"0\" /><br />\n";
		
	}
	
	// Close the book's P tag:
	echo "</p>\n";
	
}

?>
</body>
</html>
