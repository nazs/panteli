<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Using cURL</title>
</head>
<body>
<?php # Script 9.4 - curl.php

/*	This page uses cURL to post a usernamme/password
 *	combination to a password-protected Web page.
 */
 
// Identify the URL:
$url = 'http://localhost/login.php';

// Start the process:
$curl = curl_init($url);   

// Tell cURL to fail if an error occurs:
curl_setopt($curl, CURLOPT_FAILONERROR, 1); 

// Allow for redirects:
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

// Assign the returned data to a variable:
curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

// Set the timeout:
curl_setopt($curl, CURLOPT_TIMEOUT, 5);

// Use POST:
curl_setopt($curl, CURLOPT_POST, 1);

// Set the POST data:
curl_setopt($curl, CURLOPT_POSTFIELDS, 'username=username&password=password');

// Execute the transaction:
$r = curl_exec($curl);

// Close the connection:
curl_close($curl);

// Print the results:
echo '<h2>cURL Results:</h2><pre>' . htmlentities($r) . '</pre>';

?>
</body>
</html>
