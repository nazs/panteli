<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Get Stock Quotes</title>
	<style type="text/css" title="text/css" media="all">
.error {
	color: #F30;
}
.quote {
	font-weight : bold;
}
</style>
</head>
<body>
<?php # Script 9.1 - get_quote.php

/*	This page retrieves a stock price from Yahoo!.
 */

if (isset($_GET['symbol']) && !empty($_GET['symbol'])) { // Handle the form.

	// Identify the URL:
	$url = sprintf('http://quote.yahoo.com/d/quotes.csv?s=%s&f=nl1', $_GET['symbol']);

	// Open the "file".
	$fp = @fopen ($url, 'r') or die ('<div align="center" class="error">Cannot access Yahoo!</div></body></html>');
		    
	// Get the data:
	$read = fgetcsv ($fp);
		
	// Close the "file":
	fclose ($fp);
	
	// Check the results for improper symbols:
	if (strcasecmp($read[0], $_GET['symbol']) != 0) {

		// Print the results:
		echo '<div align="center">The latest value for <span class="quote">' . $read[0] . '</span> (<span class="quote">' . $_GET['symbol'] . '</span>) is $<span class="quote">' . $read[1] . '</span>.</div><br />';
		
	} else {
		echo '<div align="center" class="error">Invalid symbol!</div>';
	}

}

// Show the form:
?>
<form action="get_quote.php" method="get">
<table border="0" cellspacing="2" cellpadding="2" align="center">
	<tr align="center" valign="top">
		<td align="center" valign="top" colspan="2">Enter a NYSE stock symbol to get the latest price:</td>
	</tr>
	<tr align="center" valign="top">
		<td align="right" valign="top">Symbol:</td>
		<td align="left" valign="top"><input type="text" name="symbol" size="5" maxlength="5" /></td>
	</tr>
	<tr>
		<td align="center" valign="top" colspan="2"><input type="submit" name="submit" value="Fetch the Quote!" /></td>
	</tr>
</table>
</form>
</body>
</html>
