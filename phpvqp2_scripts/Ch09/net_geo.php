<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>PEAR::Net_Geo</title>
</head>
<body>
<?php # Script 9.3 - net_geo.php

/*	This page uses the PEAR Net_Geo class
 *	to retrieve a user's geographic location.
 */

// Include the class definition:
require_once('Net/Geo.php');

// Create the object:
$net_geo = new Net_Geo();

// Get the client's IP address:
$ip = $_SERVER['REMOTE_ADDR'];

// Get the information:
$results = $net_geo->getRecord($ip);

// Print whatever about the user:
echo "<p>Our spies tell us the following information about you:<br />
IP Address: $ip<br />
Country: {$results['COUNTRY']}<br />
City, State: {$results['CITY']}, {$results['STATE']}<br />
Latitude: {$results['LAT']}<br />
Longitude: {$results['LONG']}</p>";

// Print something about a site:
$url = 'www.entropy.ch';

// Get the IP address:
$ip = gethostbyname($url);

// Get the information:
$results = $net_geo->getRecord($ip);

// Print whatever about the URL:
echo "<p>Our spies tell us the following information about the URL $url:<br />
IP Address: $ip<br />
Country: {$results['COUNTRY']}<br />
City, State: {$results['CITY']}, {$results['STATE']}</p>";
?>
</body>
</html>

