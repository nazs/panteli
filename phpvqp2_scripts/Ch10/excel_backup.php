<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Excel Backup</title>
</head>
<body>
<?php # Script 10.5 - excel_backup.php

/*	This page uses COM to back up 
 *	a MySQL database to an Excel file.
 */

// Increase the PHP time limit:
set_time_limit(300); 

// Load the COM:
$excel = new COM ("excel.application") or die ("Cannot start Excel.</body></html>");

echo "<p>Loaded Excel Version $excel->Version</p>\n";

try {

	// Don't show the application:
	$excel->Visible = 0; 

	// Connect to the database:
	$db_name = 'mysql';
	$dbc = @mysqli_connect ('localhost', 'username', 'password', $db_name) OR die ("<p>The  database--$db_name--could not be backed up.</p>\n</body>\n</html>\n");

	// Retrieve the tables:
	$q = 'SHOW TABLES';
	$r = mysqli_query($dbc, $q);
	
	// Back up if at least one table exists:
	if (mysqli_num_rows($r) > 0) {
	
		// Indicate what is happening:
		echo "<p>Backing up database '$db_name'.</p>\n";
		
		// Create a new workbook:
		$workbook = $excel->Workbooks->Add();
		
		// Go ahead and save the file:
		$workbook->SaveAs("C:\Documents and Settings\Larry Ullman\Desktop\db_backup.xls");
	
		// Each table gets its own sheet:
		$sheet_number = 1;
	
		// Fetch each table name.
		while (list($table) = mysqli_fetch_array($r, MYSQLI_NUM)) {
		
			// Get the records for this table:
			$q2 = "SELECT * FROM $table";
			$r2 = mysqli_query($dbc, $q2);
			
			// Back up if records exist:
			if (mysqli_num_rows($r2) > 0) {
	
				// Add the sheet:
				$sheet = 'Sheet' . $sheet_number;
				if ($sheet_number > 3) $workbook->Sheets->Add;
				$worksheet = $workbook->Worksheets($sheet); 
				$worksheet->Activate;
				$worksheet->Name = $table;
	
				// Start at row 1 for each table:
				$excel_row = 1;
				
				// Fetch all the records for this table:
				while ($row = mysqli_fetch_array($r2, MYSQLI_NUM)) {
	
					// Each record starts in the first column:
					$excel_col = 1;
	
					// Write the data to the spreadsheet:
					foreach ($row as $value) { 
					 
					 	// Reference the cell:
						$cell = $worksheet->Cells($excel_row,$excel_col);
						
						// Need to change the formatting if
						// the data isn't numeric:
						if (is_numeric($value)) {
							$cell->Value = $value;
						} else {
							$cell->NumberFormat = '@';
							$cell->Value = $value;
						}
						
						// Increase the column:
						$excel_col++;			
						
					} // End of FOREACH.
					
					// Increase the row:
					$excel_row++;
	
				} // End of table WHILE loop.
				
				// Print the success:
				echo "<p>Table '$table' backed up.</p>\n";
				
				// Increase the sheet number:
				$sheet_number++;
				
				// Save the workbook:
				$workbook->Save();
	
			} // End of mysqli_num_rows() IF.
					
		} // End of WHILE loop.

		// Quit the application.
		$excel->Quit();
	
	} else { // No tables to backup!
		echo "<p>The submitted database--$db_name--contains no tables.</p>\n";
	}

} catch (com_exception $e) { // Catch COM exceptions.
	echo "<p>$e</p>";	
} catch (exception $e) { // Catch other exceptions.
	echo '<p>' . $e->getMessage() . '</p>';
}

?>
<body>
<html>
