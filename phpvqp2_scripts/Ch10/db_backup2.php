<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Database Backup</title>
</head>
<body>
<?php # Script 10.2 - db_backup2.php

/*	This page retrieves all the data from a database
 *	and writes that data to a text file.
 *	The text file is NOT compressed.
 */
 
// Establish variables and setup:
$db_name = 'test';

// Backup directory:
$dir = "backups/$db_name";

// Make the database-specific directory, if it doesn't exist.
if (!is_dir($dir)) {
	if (!@mkdir($dir)) {
		die ("<p>The backup directory--$dir--could not be created.</p>\n</body>\n</html>\n");
	}
}

// Get the current time for using in all filenames:
$time = time();

// Connect to the database:
$dbc = @mysqli_connect ('localhost', 'username', 'password', $db_name) OR die ("<p>The  database--$db_name--could not be backed up.</p>\n</body>\n</html>\n");

// Retrieve the tables:
$q = 'SHOW TABLES';
$r = mysqli_query($dbc, $q);

// Back up if at least one table exists:
if (mysqli_num_rows($r) > 0) {

	// Indicate what is happening:
	echo "<p>Backing up database '$db_name'.</p>\n";
	
	// Fetch each table name.
	while (list($table) = mysqli_fetch_array($r, MYSQLI_NUM)) {
	
		// Get the records for this table:
		$q2 = "SELECT * FROM $table";
		$r2 = mysqli_query($dbc, $q2);
		
		// Back up if records exist:
		if (mysqli_num_rows($r2) > 0) {

			// Attempt to open the file:
			if ($fp = fopen ("$dir/{$table}_{$time}.sql", 'w')) {
			
				// Fetch all the records for this table:
				while ($row = mysqli_fetch_array($r2, MYSQLI_NUM)) {

					// Write the data as a comma-delineated row:
					foreach ($row as $value) { 
						
						fwrite ($fp, "'$value', ");
					}
					
					// Add a new line to each row:
					fwrite ($fp, "\n"); 

				} // End of WHILE loop.

				// Close the file:
				fclose ($fp); 
			
				// Print the success:
				echo "<p>Table '$table' backed up.</p>\n";

			} else { // Could not create the file!
				echo "<p>The file--$dir/{$table}_{$time}.sql--could not be opened for writing.</p>\n";
				break; // Leave the WHILE loop.
			} // End of fopen() IF.

		} // End of mysqli_num_rows() IF.
		
	} // End of WHILE loop.

} else {
	echo "<p>The submitted database--$db_name--contains no tables.</p>\n";
}

?>
</body>
</html>
