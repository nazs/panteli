<?php # Script 10.3 - calculator.php

/*	This page uses GTK to create 
 *	a graphical calculator.
 */

// Make sure that the GTK module has been loaded:
if (!class_exists('gtk')) die('The PHP-Gtk2 module has not been loaded!');

// Function for resetting the calculator:
function clear () {
 
 	// Reset the vars:
	global $n1, $n2, $operator;
	$n1 = false;
	$n2 = false;
	$operator = false;
	
	// Clear the display:
	set_display ();

} // End of clear() function.

// Function for displaying value in the calculator 'window':
function set_display ($value = 0) {
	global $display;
	$display->set_text ($value);
} // End of set_display() function.

// The calculate() function does the actual math:
function calculate () {
 
	global $n1, $n2, $operator;
	
	// Set initial value, just in case:
	$value = $n1;

	// What mathematical operation?
	switch ($operator) {
		case 'add':
			$value = $n1 + $n2;
			break;
		case 'subtract':
			$value = $n1 - $n2;
			break;
		case 'multiply':
			$value = $n1 * $n2;
			break;
		case 'divide':
			$value = $n1 / $n2;
			break;
	}

	 // Display the calculated value:	
	set_display ($value);
	
	// Reset the values:
	$n1 = $value;
	$operator = false;
	$n2 = false;
	
} // End of calculate() function.

// Function for assigning the operator being used:
function set_operator ($which) {
	global $operator;
	
	// If the $operator is already set,
	// calculate using the current values.
	if ($operator) calculate();
	
	$operator = $which;

} // End of set_operator() function.

// Function for assigning values:
function set_number ($value) {

	global $n1, $n2, $operator;
	
	// Concatenate to either the $n1 or $n2 value:
	if (!$operator) {
		$n1 .= $value;
		set_display($n1);
	} else {
		$n2 .= $value;
		set_display($n2);
	}
}

// *******************
// End of Functions
// *******************

// Define the main variables:
$n1 = false; 
$n2 = false;
$operator = false;

// Create a new window:
$window = new GtkWindow();
$window->set_title ('Calculator');
$window->set_default_size (320, 320);

// Create another container:
$box = new GtkVBox();
$window->add($box);

// Make a table:
$table = new GtkTable(5, 6);
$table->set_row_spacings(2);
$table->set_col_spacings(2);
$table->set_border_width(5);

// Put the table into the box:
$box->pack_start($table);

// Make a display:
$display = new GtkLabel('display');
$table->attach($display, 1, 4, 1, 2);

// Make the 0-9 buttons.
for ($i = 0; $i <= 9; $i++) {
	
	// Determine the table coordinates
	// for each number:
	switch ($i) {
		case 0:
			$x = 1;
			$y = 5;
			break;
		case 1:
			$x = 1;
			$y = 4;
			break;
		case 2:
			$x = 2;
			$y = 4;
			break;
		case 3:
			$x = 3;
			$y = 4;
			break;
		case 4:
			$x = 1;
			$y = 3;
			break;
		case 5:
			$x = 2;
			$y = 3;
			break;
		case 6:
			$x = 3;
			$y = 3;
			break;
		case 7:
			$x = 1;
			$y = 2;
			break;
		case 8:
			$x = 2;
			$y = 2;
			break;
		case 9:
			$x = 3;
			$y = 2;
			break;
	}
	
	// Make the button for the number:
	$button = new GtkButton($i);
	$button->connect_simple ('clicked', 'set_number', $i);
	$table->attach($button, $x, ($x+1), $y, ($y+1));
	
} // End of 0-9 FOR loop.


// Place the remaining buttons...

// Decimal point:
$decimal = new GtkButton('.');
$decimal->connect_simple ('clicked', 'set_number', '.');
$table->attach($decimal, 2, 3, 5, 6);

// Equals sign:
$equals = new GtkButton('=');
$equals->connect_simple ('clicked', 'calculate');
$table->attach($equals, 3, 4, 5, 6);

// Clear:
$clear = new GtkButton('C');
$clear->connect_simple ('clicked', 'clear');
$table->attach($clear, 4, 5, 1, 2);

// Plus sign:
$add = new GtkButton('+');
$add->connect_simple ('clicked', 'set_operator', 'add');
$table->attach($add, 4, 5, 2, 3);

// Minus sign:
$subtract = new GtkButton('-');
$subtract->connect_simple ('clicked', 'set_operator', 'subtract');
$table->attach($subtract, 4, 5, 3, 4);

// Multiplication sign:
$multiply = new GtkButton('*');
$multiply->connect_simple ('clicked', 'set_operator', 'multiply');
$table->attach($multiply, 4, 5, 4, 5);

// Division sign:
$divide = new GtkButton('/');
$divide->connect_simple ('clicked', 'set_operator', 'divide');
$table->attach($divide, 4, 5, 5, 6);

// Reset the calculator to start:
clear();

// Connect the quit function:
$window->connect_simple ('destroy', array('Gtk', 'main_quit'));

// Show everything:
$window->show_all();

// Start the application:
Gtk::main();
?>
