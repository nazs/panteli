<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Filter</title>
	<style type="text/css" title="text/css" media="all">
.error {
	color: #F30;
}
</style>
</head>
<body>
<?php # Script 4.2 - filter.php

/*	This page uses the Filter functions
 *	to validate form data.
 *	This page will print out the filtered data.
 */
 
if (isset($_POST['submitted'])) { // Handle the form.

	// Sanitize the name:
	$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
	if ($name) {
		echo "<p>Name: $name<br />\$_POST['name']: {$_POST['name']}</p>\n";
	} else {
		echo '<p class="error">Please enter your name.</p>';
	}

	// Validate the email address using FILTER_VALIDATE_EMAIL:
	$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
	if ($email) {
		echo "<p>Email Address: $email</p>\n";
	} else {
		echo '<p class="error">Please enter your email address.</p>';
	}
	
	// Validate the ICQ number using FILTER_VALIDATE_INT:
	$icq = filter_input(INPUT_POST, 'icq', FILTER_VALIDATE_INT);
	if ($icq) {
		echo "<p>ICQ Number: $icq</p>\n";
	} else {
		echo '<p class="error">Please enter your ICQ number.</p>';
	}
	
	// Strip tags and encode quotes:	
	$comments = filter_input(INPUT_POST, 'comments', FILTER_SANITIZE_STRING);
	if ($comments) {
		echo "<p>Comments: $comments<br />\$_POST['comments']: {$_POST['comments']}</p>\n";
	} else {
		echo '<p class="error">Please enter your comments.</p>';
	}
		
} // End of $_POST['submitted'] IF.

// Show the form.
?>
<form method="post" action="filter.php">
<fieldset>
<legend>Registration Form</legend>
<p>Name: <input type="text" name="name" /></p>
<p>Email Address: <input type="text" name="email" /></p>
<p>ICQ Number: <input type="text" name="icq" /></p>
<p>Comments: <textarea name="comments" rows="5" cols="40"></textarea></p>

<input type="hidden" name="submitted" value="true" />
<input type="submit" name="submit" value="Submit" />
</fieldset>
</form>

</body>
</html>
