<?php # Script 4.5 - set_mcrypt_cookie.php

/*	This page uses the MCrypt library
 *	to encrypt some data.
 *	The data will then be stored in a cookie,
 *	as will the encryption IV.
 */

// Create the key:
$key = md5('77 public drop-shadow Java');

// Data to be encrypted:
$data = 'rosebud';

// Open the cipher:
// Using Rijndael 256 in CBC mode.
$m = mcrypt_module_open('rijndael-256', '', 'cbc', '');

// Create the IV:
// Use MCRYPT_RAND on Windows instead of MCRYPT_DEV_RANDOM.
$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($m), MCRYPT_DEV_RANDOM);

// Initialize the encryption:
mcrypt_generic_init($m, $key, $iv);

// Encrypt the data:
$data = mcrypt_generic($m, $data);

// Close the encryption handler:
mcrypt_generic_deinit($m);

// Close the cipher:
mcrypt_module_close($m);

// Set the cookies:
setcookie('thing1', base64_encode($data));
setcookie('thing2', base64_encode($iv));
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>A More Secure Cookie</title>
</head>
<body>
<p>The cookie has been sent. Its value is '<?php echo base64_encode($data); ?>'.</p>
</body>
</html>
