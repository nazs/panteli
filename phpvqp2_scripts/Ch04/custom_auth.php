<?php # Script 4.4 - custom_auth.php

/* 	This page uses PEAR Auth to control access.
 *	This assumes a database called "auth",
 *	accessible to a MySQL user of "username@localhost" 
 *	with a password of "password".
 *	Table definition:

	CREATE TABLE users (
	user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	email VARCHAR(60) NOT NULL,
	pass CHAR(40) NOT NULL,
	first_name VARCHAR (20) NOT NULL,
	last_name VARCHAR(40) NOT NULL,
	PRIMARY KEY (user_id),
	UNIQUE (email),
	KEY (email, pass)
	)
	
 *	SHA1() is used to encrypt the passwords.
 */

// Need the PEAR class:
require_once ('Auth.php');

// Function for showing a login form:
function show_login_form() {

    echo '<form method="post" action="custom_auth.php">
<p>Email <input type="text" name="username" /></p>
<p>Password <input type="password" name="password" /></p>
<input type="submit" value="Login" />
</form><br />
';

} // End of show_login_form() function.

// All options:
// Use specific username and password columns.
// Use SHA1() to encrypt the passwords.
// Retrieve all fields.
$options = array(
'dsn' => 'mysql://username:password@localhost/auth',
'table' => 'users',
'usernamecol' => 'email',
'passwordcol' => 'pass',
'cryptType' => 'sha1',
'db_fields' => '*'
);

// Create the Auth object:
$auth = new Auth('DB', $options, 'show_login_form');

// Add a new user:
$auth->addUser('me@example.com', 'mypass', array('first_name' => 'Larry', 'last_name' => 'Ullman'));

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Restricted Page</title>
</head>
<body>
<?php

// Start the authorization:
$auth->start();

// Confirm authorization:
if ($auth->checkAuth()) {
	
	// Print the user's name:
	echo "<p>You, {$auth->getAuthData('first_name')} {$auth->getAuthData('last_name')}, are logged in and can read this. How cool is that?</p><p><a href=\"?action=logout\">Logout</a></p>";
	
} else { // Unauthorized.
	
	echo '<p>You must be logged in to access this page.</p>';
	
}

?>
</body>
</html>
