<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>A More Secure Cookie</title>
</head>
<body>
<?php # Script 4.6 - read_mcrypt_cookie.php

/*	This page uses the MCrypt library
 *	to decrypt data stored in a cookie.
 */

// Make sure the cookies exist:
if (isset($_COOKIE['thing1']) && isset($_COOKIE['thing2'])) {

	// Create the key:
	$key = md5('77 public drop-shadow Java');
	
	// Open the cipher:
	// Using Rijndael 256 in CBC mode.
	$m = mcrypt_module_open('rijndael-256', '', 'cbc', '');
	
	// Decode the IV:
	$iv = base64_decode($_COOKIE['thing2']);
	
	// Initialize the encryption:
	mcrypt_generic_init($m, $key, $iv);
	
	// Decrypt the data:
	$data = mdecrypt_generic($m, base64_decode($_COOKIE['thing1']));
	
	// Close the encryption handler:
	mcrypt_generic_deinit($m);
	
	// Close the cipher:
	mcrypt_module_close($m);
	
	// Print the data.
	echo '<p>The cookie has been received. Its value is "' . trim($data) . '".</p>';

} else { // No cookies!
	echo '<p>There\'s nothing to see here.</p>';
}
?>
</body>
</html>
