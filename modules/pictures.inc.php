<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>

<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<main class="row">
		<section class="col-sm-8 col-sm-push-4 gen-pad">	
			<div class="modalphotos gen-pad">
				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Staff Images</h3>
				  </div>
				  <div class="panel-body">
					<img src="img/laki_sm.jpg" alt="Vasilakis Panteli - Managing Director" >
					<img src="img/marina_sm.jpg" alt="Marina Panteli - Director and Company Secretary" >
					<img src="img/andrew_sm.jpg" alt="Andrew Panteli - Director" >
				  </div>
				</div>	
			</div>		

		

	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Restaurant Images</h3>
	  </div>
	  <div class="panel-body">
		<a href="img/rest1.jpg" class="swipebox" title="Restaurant Image 1" rel="restaurant"><img src="img/rest1_sm.jpg" alt="Restaurant Image 1"></a>
		<a href="img/rest2.jpg" class="swipebox" title="Restaurant Image 2" rel="restaurant"><img src="img/rest2_sm.jpg" alt="Restaurant Image 2"></a>
		<a href="img/rest3.jpg" class="swipebox" title="Restaurant Image 3" rel="restaurant"><img src="img/rest3_sm.jpg" alt="Restaurant Image 3"></a>
		<a href="img/rest4.jpg" class="swipebox" title="Restaurant Image 4" rel="restaurant"><img src="img/rest4_sm.jpg" alt="Restaurant Image 4"></a>
		<a href="img/rest5.jpg" class="swipebox" title="Restaurant Image 5" rel="restaurant"><img src="img/rest5_sm.jpg" alt="Restaurant Image 5"></a>
		<a href="img/rest6.jpg" class="swipebox" title="Restaurant Image 6" rel="restaurant"><img src="img/rest6_sm.jpg" alt="Restaurant Image 6"></a>
		<a href="img/rest7.jpg" class="swipebox" title="Restaurant Image 7" rel="restaurant"><img src="img/rest7_sm.jpg" alt="Restaurant Image 7"></a>
		<a href="img/rest8.jpg" class="swipebox" title="Restaurant Image 8" rel="restaurant"><img src="img/rest8_sm.jpg" alt="Restaurant Image 8"></a>
		<a href="img/rest9.jpg" class="swipebox" title="Restaurant Image 9" rel="restaurant"><img src="img/rest9_sm.jpg" alt="Restaurant Image 9"></a>
		<a href="img/rest10.jpg" class="swipebox" title="Restaurant Image 10" rel="restaurant"><img src="img/rest10_sm.jpg" alt="Restaurant Image 10"></a>
	  </div>
	</div>	
		


		</section>	
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</main>
</div>

<!-- MODAL CONTACT FORM -->
<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-body">
				<img src="" alt="Modal Photo" id="modalimage" class="img-responsive cen-blo">
				<div class="horcenter" id="imagetag"></div>
			</div>
		</div> <!-- modal-content -->
	</div> <!-- modal-dialog -->
</div> <!-- end modal -->

