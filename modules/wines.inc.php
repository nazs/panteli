<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("assets/ales.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed" >';
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/wines.php target=_blank>Print Menu</a></span></th></tr>';
$output .= "<tr><td></td><td colspan='2'><i>We take great pleasure in stocking a selection of Ales from local breweries. Here we list some of our regulars. Due to the nature of these products we cannot always guarantee availability. Specials will be available throughout the year.</i></td></tr>";
$count = 1;
$old = '';

foreach ($json['menu'] as $snack) {
	$no = $snack['no']; $grp = $snack['group'];
	switch ($grp) {
		case '1':
			$name = 'RAMSGATE BREWERY';
			break;
		case '2':
			$name = 'WHITSTABLE BREWERY';
			break;
		case '3':
			$name = 'SHEPHERD NEAME';
			break;
		case '4':
			$name = 'PINE TREES FARM';
			break;
		case '5':
			$name = 'BARNSOLE VINEYARD';
			break;
		case '6':
			$name = 'OTHERS';
			break;

	}
	if($old < $grp) { 
		$output .= '<tr ><td class="header pad" colspan="2">'.$name.'</td></tr>';
		if($name == 'PINE TREES FARM') { $output .= '<tr><td></td><td>DUDDA\'S TUN CIDER: KATY clean, crisp tasting farm-made cider using 100% apple juice grown at Pine Trees Farm</td><td></td></tr>';
		}		
	}

	$output .= "<tr><td class='nos'>".$count."</td>";
	$output .= "<td>". $snack['item']. "</td>";
	$output .= "<td class='price'>&pound; ". number_format($snack['price'],2). "</td></tr>";
	$old = $grp;
	$count ++;


}
	$output .= "</table>";
	echo $output;
?>	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


