<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("assets/adult.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">';
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/adult.php target=_blank>Print Menu</a></span></th></tr>';
$count = 1;
foreach ($json['menu'] as $snack) {

	$output .= "<tr>";
	$output .= "<td width='70px'>". $snack['item']. "</td>";
	$output .= "<td>". $snack['description']. "</td>";
	$output .= "<td class='price'>&pound; ". number_format($snack['price'],2). "</td></tr>";
	$count ++;

}
	$output .= "<tr height='15px'><td colspan='3'></td></tr><tr><td></td><td colspan='2'><i>Please phone Canterbury (01227) 765506 to book. Fax also available on (01227) 765506.These are just a few suggestions. We are able to cater for your exact requirements. Please ask for a quotation.</i></td></tr>";
	$output .= "</table>";
	echo $output;
?>	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


