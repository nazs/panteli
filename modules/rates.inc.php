<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;
?>

<section class="container">
	<div class="row">
		<main class="col-lg-9 col-md-9 col-sm-9 col-lg-push-3 col-md-push-3 col-sm-push-3 col-xs-push-3 clearfix">
		<div class="container"><img src="img/header2.jpg" alt="" class="img-responsive img-rounded"></div>
		
		<h2>Villa Rental Rates 2009 / 2010</h2>
			  
			  <table class="rates" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                  
                  <tr height="22">
                    <td>&nbsp;</td>
                    <td><p align="center"><strong>$</strong></p></td>
                    <td><p align="center"><strong>£</strong></p></td>
                    <td><p align="center"><strong>$</strong></p></td>
                    <td><p align="center"><strong>£</strong></p></td>
                  </tr>
                  <tr height="22">
                    <td><p><strong> &nbsp;</strong></p></td>
                    <td><p align="center"><strong>Daily</strong></p></td>
                    <td><p align="center"><strong>Daily</strong></p></td>
                    <td><p align="center"><strong>Weekly</strong></p></td>
                    <td><p align="center"><strong>Weekly</strong></p></td>
                  </tr>
                  <tr height="22">
                    <td><p><strong>Low</strong></p></td>
                    <td><p align="center">122.00</p></td>
                    <td><p align="center">75.78</p></td>
                    <td><p align="center">850.00</p></td>
                    <td><p align="center">527.95</p></td>
                  </tr>
                  <tr height="22">
                    <td><p><strong>Med</strong></p></td>
                    <td><p align="center">130.00</p></td>
                    <td><p align="center">80.75</p></td>
                    <td><p align="center">910.00</p></td>
                    <td><p align="center">565.22</p></td>
                  </tr>
                  <tr height="22">
                    <td><p><strong>High</strong></p></td>
                    <td><p align="center">140.00</p></td>
                    <td><p align="center">86.96</p></td>
                    <td><p align="center">980.00</p></td>
                    <td><p align="center">608.70</p></td>
                  </tr>
                  <tr height="22">
                    <td><p><strong>Peak</strong></p></td>
                    <td><p align="center">145.00</p></td>
                    <td><p align="center">90.06</p></td>
                    <td><p align="center">1015.00</p></td>
                    <td><p align="center">630.43</p></td>
                  </tr>
                </tbody>
		    </table>
		    <p>Peak = Easter & Christmas
			<p><strong>Booking deposit</strong></p>
			<p>£200 ($350) up to 2 weeks and £300 ($500) for 3 weeks or longer (non refundable)
			Pool Heat £13 ($22) per day or £90 ($150) per week, Recommended October to March.</p>
			<p>To check availability and for reservations call +44 7838 092351 or email @hotmail.co.uk for further information.</p>
			<p><strong>Last minute booking discounts subject to availability.</strong></p>
		</main>
		<?php include_once 'inc/aside.php'; ?>			
	</div>
</section> <!-- container -->