<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php			
$jsondata = file_get_contents("assets/rest.json");
$json = json_decode($jsondata, true);

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}
array_sort_by_column($json['menu'], 'group');

$output = '<table class="table table-condensed">';
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/rest.php target=_blank>Print Menu</a></span></th></tr>';
//$output .= "<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>";
$count = 1;
$old = '';
foreach ($json['menu'] as $snack) {
	$no = $snack['no']; $grp = $snack['group'];
	$price = number_format($snack['price'], 2);

	if($old != $grp) { $output .= '<tr height=15><td colspan="3"></td></tr>';}

	$output .= "\t<tr><td class='nos'>".$count."</td>";
	$output .= "<td>". $snack['item']. "</td>";
	$output .= "<td class='price'>&pound; ". $price. "</td></tr>\n";

	$old = $grp; // grab group # to compare to next one
	$count ++;


}
	$output .= "</table>";
	echo $output;
?>

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


