<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
if(isset($_SESSION['submitted'])) {
	echo $_SESSION['submitted'];
	unset($_SESSION['submitted']);
}
?>


<div class="container">
	<div class="row">

		<main class="col-sm-8 col-sm-push-4 hometxt">
			<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?msa=0&amp;msid=201527909363085357837.0004f72698ea4da1b7ebf&amp;ie=UTF8&amp;t=m&amp;ll=51.225862,1.40272&amp;spn=0.003225,0.006866&amp;z=17&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.com/maps/ms?msa=0&amp;msid=201527909363085357837.0004f72698ea4da1b7ebf&amp;ie=UTF8&amp;t=m&amp;ll=51.225862,1.40272&amp;spn=0.003225,0.006866&amp;z=17&amp;source=embed" style="color:#0000FF;text-align:left">My Saved Places</a> in a larger map</small>
	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


