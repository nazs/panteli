<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>

<main class="container">
	<div class="jumbotron">
		<section class="row">
			<div class="col-md-6">
				<img src="img/dishes.jpg"  alt="" class="img-responsive img-rounded ">
			</div>
			<div class="col-md-6"><h2>Authentic Thai Cuisine</h2>
				<h3 class="">Thailand, the land of smiles and its unique cuisine is the Buddhist land in South East Asia, the country which is formerly known as Siam has been renamed Thailand since 1939 in the era of King Rama VIII. Thailand means the land of freedom. The Thais are proud of their country, which has never been a colony of any country. The capital city of Thailand is Bangkok.</h3>
			</div>
		</section>
	</div>
</main>		
<aside class="container">
	<section class="row">
		<div class="col-sm-3">	
			<img src="img/dish01.jpg" alt="" class="img-responsive img-circle center-block">
		</div>
		<div class="col-sm-3">
			<img src="img/dish02.jpg" alt="" class="img-responsive img-circle center-block">
		</div>

		<div class="col-sm-3">
			<img src="img/dish03.jpg" alt="" class="img-responsive img-circle center-block">
		</div>
		<div class="col-sm-3">
			<img src="img/dish04.jpg" alt="" class="img-responsive img-circle center-block">
		</div>
	</section>
</aside>		
	

<!-- <main class="container">
	<div class="row">
		
		<section class="col-sm-3">
			<ul class="nav nav-tabs nav-stacked">
				<li><a href="" Link="" 1="">Link1</a></li>
				<li><a href="" Link="" 2="">Link2</a></li>
				<li><a href="" Link="" 3="">Link3</a></li>
			</ul>
		</section>
		<section class="col-sm-8 col-sm-offset-1">
			
				<div class="">
					<img src="img/Bangkok_Pavilion_home.jpg" alt="" class=" img-responsive img-circle">
					<p>Thailand, the land of smiles and its unique cuisine is the Buddhist land in South East Asia, the country which is formerly known as Siam has been renamed Thailand since 1939 in the era of King Rama VIII. Thailand means the land of freedom. The Thais are proud of their country, which has never been a colony of any country. The capital city of Thailand is Bangkok.</p>

				</div>
			
		</section>

	</div>
</main> -->
<!-- <h5>.

<blockquote>Thai cuisine ranges from mild and subtly flavoured dishes to hot and fiery ones.</blockquote> The food is flavoured with many different herbs and spices: garlic, lemon grass, galangal, ginger, basil leaves etc. Thai curry paste is prepared by blending at least nine different ingredients. Thailand has a long and beautiful coastline as well as wealthy seafood dishes.

The traditional way to eat Thai food is with a spoon and fork from a flat plate. The plate filled with rice are taken together with main course dishes. The Thai’s prefer not to mix their food in order to retain the subtleties of each dish’s taste and flavour. To get the best out of your Thai meal, one may also share a variety of starters, salads and vegetable dishes.

We take great pride in preparing Thai food and that’s why each dish is filled with the work of art.

We hope you will enjoy your dining experience with us. </p> -->


