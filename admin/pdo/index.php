<?php

//print_r(PDO::getAvailabledrivers()); // Array ( [0] => mysql [1] => sqlite ) 

try {
	$handler = new PDO('mysql:host=127.0.0.1;dbname=resources', 'root', '');	
	$handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {
	//die('Sorry, DB error.');
	echo $e->getMessage(); // "SQLSTATE[HY000] [1049] Unknown database 'tests' "
	die();

}

class Resources {
	public $id, $quote, $quote_date;

	public function __construct() {
		$this->quote = "{$this->quote_date}: {$this->quote} ";
	}
}

//echo 'The rest of our page.';

$query = $handler->query('SELECT * from quotes');
$query->setFetchMode(PDO::FETCH_CLASS, 'Resources');

//$r = $query->fetch(); // same as doing $query->fetch(PDO::FETCH_BOTH) 
//$r = $query->fetch(PDO::FETCH_OBJ); 
//echo '<pre>', print_r($r), '</pre>';

while ($r = $query->fetch()) {
	//echo '<pre>', print_r($r), '</pre>';
	echo $r->quote, '<br>';
}


echo '<hr>';
// to check results
$query = $handler->query('SELECT * FROM quotes LIMIT 0');
$results = $query->fetchAll(PDO::FETCH_ASSOC);

if(count($results)) {
	echo 'There are results.';
} else {
	echo 'There are no results.';
}
// END to check results
echo '<hr>';

?>