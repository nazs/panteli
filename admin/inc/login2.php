  <form action="<?php echo $_SERVER['PHP_SELF']?>" role="form" method="post" id="admin-login">
    <div class="form-group">
      <label for="exampleInputEmail1">User ID</label>
      <input type="text" class="form-control" id="uid" placeholder="Enter user ID" name="uid">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="pwd" placeholder="Password" name="pwd">
    </div>
    <span id="login-err"></span>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>