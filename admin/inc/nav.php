<?php
	if(isset($_SESSION['admin']))
	{
		$logout = "<li><a href='index.php?p=logout&now'>Logout</a></li>";
		$logout .= "<li><a href='' data-toggle='modal' data-target='#passChange'>Change Password</a></li>";

	} else 
	{
		$logout = '';
	}
?>

	<img src="../img/front.jpg" class="hidden-xs img-rounded ">
	<ul class="nav nav-pills nav-stacked">
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=home') echo 'class="active"' ?>><a href="index.php?p=home">Home</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Self-Service') echo 'class="active"' ?>><a href="index.php?p=Self-Service">Self-Service Menu</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Restaurant') echo 'class="active"' ?>><a href="index.php?p=Restaurant">Restaurant Menu</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Student-Groups') echo 'class="active"' ?>><a href="index.php?p=Student-Groups">Student Groups Menu</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Adult-Groups') echo 'class="active"' ?>><a href="index.php?p=Adult-Groups">Adult Groups Menu</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Breakfasts') echo 'class="active"' ?>><a href="index.php?p=Breakfasts">Breakfasts</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Group-Breakfasts-and-Packed-Lunches') echo 'class="active"' ?>><a href="index.php?p=Group-Breakfasts-and-Packed-Lunches">Group Breakfasts and Packed Lunches</a></li>
		<li <?php if($_SERVER['QUERY_STRING'] == 'p=Wines-and-Beers') echo 'class="active"' ?>><a href="index.php?p=Wines-and-Beers">Wines and Beers</a></li>
		<?php echo $logout; ?>
	</ul>
	
	
<?php

?>