<?php
$err = '';
if(isset($_SESSION['error'])) { 
  $err = $_SESSION['error']; 
  unset($_SESSION['error']); 
}

?>
 <form action="index.php?p=process-login" role="form" method="post" id="admin-login">
    <div class="form-group">
      <label for="exampleInputEmail1">User ID</label>
      <input type="text" class="form-control" id="uid" placeholder="Enter user ID" name="uid">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="pwd" placeholder="Password" name="pwd">
    </div>
    <span id="login-err"><?php echo $err;?></span>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>

<?php
/*$error = isset($_SESSION['error']) ? 'Login error' : '';
echo $error;
unset($_SESSION['error']);*/

?>
