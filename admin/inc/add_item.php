<?php
//echo $_SERVER['QUERY_STRING'];
$process = $_SERVER['QUERY_STRING'] == 'p=Restaurant' ? 'process-add-rest' : 'process-add-wines';

$no = $_SESSION['no'];

	echo <<<EOD
		
	<div class="modal " id="add-item">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times;</button>
					<h3 class='pophere'></h3>
				</div>
				<div class="modal-body">
					<form action="index.php?p=$process" role="form" id="add-item" method="post">
	
									
						<div class="form-group">
							<label for="contact-message">Item name:</label><span id="msgReq" class="frm-err pull-right"></span>
							<div class="controls">
								<textarea name="item" class="form-control" id="new-item" cols="30" rows="2" wrap="hard"></textarea>
							</div>
						</div>	

						<div class="form-group">
							<div class="controls">
								<div class="row">
									<label class="col-xs-8">Price:</label>
									<div class="col-xs-4">
										 <input size=5 type="text" id="new-price" name="price" class="form-control input-sm">
										 <input type="hidden" name="group" id='group'>
										 <input type="hidden" name="no" value="$no">
									</div>
								</div>
							</div>
						</div>			


						<div class="form-group">
							<div class="controls">
								<input type="submit" class="btn btn-primary" value="Add item"> <span id="frm-err"></span>
								 <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button> <span class='add-err'></span>
							</div>
						</div>
					</form>
				</div>
			</div> <!-- modal-content -->
		</div> <!-- modal-dialog -->
	</div> <!-- end modal -->
EOD;

?>

<div class="modal " id="passChange">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h3>Password Change</h3>
			</div>
			<div class="modal-body">
				<form action="index.php?p=password" role="form" id="contact-form" method="post">									
					<div class="form-group">
						<label for="newPass">New Password:</label><span id="msgReq" class="frm-err pull-right"></span>
						<div class="controls">
							<input type="password" id="newPass" name="newPass" class="form-control" id="newPass"  >
						</div>
					</div>										
					<div class="form-group">
						<label for="confirmPass">Confirm Password:</label><span id="msgReq" class="frm-err pull-right"></span>
						<div class="controls">
							<input type="password" id="confirmPass" name="confirmPass" class="form-control" id="confirmPass" >
							<input type="hidden" name="uid" class="form-control" value="<?php echo $_SESSION['uid'] ;?>" >
						</div>
					</div>	
					<div class="form-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary" value="Update"> <span id="pass-err"></span>
							 <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div> <!-- modal-content -->
	</div> <!-- modal-dialog -->
</div> <!-- end modal -->