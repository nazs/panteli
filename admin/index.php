<?php # Script 2.4 - index.php
if(!isset($_GET['p'])) header ("Location: index.php?p=home");
/* 
 *	This is the main page.
 *	This page includes the configuration file, 
 *	the templates, and any content-specific modules.
 */
// Require the configuration file before any PHP code:
require_once ('inc/config.inc.php');

// Validate what page to show:
if (isset($_GET['p'])) {
	$p = $_GET['p'];
} elseif (isset($_POST['p'])) { // Forms
	$p = $_POST['p'];
} else {
	$p = NULL;
}
// Determine what page to display:
$more_style = '';

//if(!strpos($_SERVER['PHP_SELF'], 'index.php'))
//{	if($_SESSION['t_user_id'] == FALSE ) header("Location: index.php");}

switch ($p) {

	case 'home':
		$page = 'home.inc.php';
		$page_title = 'Welcome';
		$img = '';
		$more_style = '';
		break;
	case 'Self-Service':
		$page = 'self.inc.php';
		$page_title = 'Self-Service Menu';
		$img = '';
		$more_style = '';
		break;	

	case 'Restaurant':
		$page = 'rest.inc.php';
		$page_title = 'Restaurant Menu';
		$img = '';
		$more_style = '';
		break;	

	case 'Student-Groups':
		$page = 'stud.inc.php';
		$page_title = 'Student Groups Menu';
		$img = '';
		$more_style = '';
		break;	

	case 'Adult-Groups':
		$page = 'adult.inc.php';
		$page_title = 'Adult Groups Menu';
		$img = '';
		$more_style = '';
		break;				

	case 'Breakfasts':
		$page = 'break.inc.php';
		$page_title = 'Breakfast Menu';
		$img = '';
		$more_style = '';
		break;

	case 'Group-Breakfasts-and-Packed-Lunches':
		$page = 'groupbreakfasts.inc.php';
		$page_title = 'Group Breakfasts and Packed Lunches';
		$img = '';
		$more_style = '';
		break;	
	
	case 'Wines-and-Beers':
		$page = 'wines.inc.php';
		$page_title = 'Wines and Beers';
		$img = '';
		$more_style = '';
		break;	

	case 'process-login':
		$page = 'process-login.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'snacks':
		$page = 'process-snacks.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'child':
		$page = 'process-child.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'hotdrinks':
		$page = 'process-hotdrinks.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'colddrinks':
		$page = 'process-colddrinks.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'desserts':
		$page = 'process-desserts.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'students':
		$page = 'process-student.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'restaurant':
		$page = 'process-rest.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'adult':
		$page = 'process-adult.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'group_breakfast':
		$page = 'process-group_breakfasts.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'process-add-rest':
		$page = 'process-add-rest.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'process-add-wines':
		$page = 'process-add-wines.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'wines':
		$page = 'process-wines.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'break':
		$page = 'process-break.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'password':
		$page = 'process-login.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	

	case 'logout':
		$page = 'process-login.inc.php';
		$page_title = '';
		$img = '';
		$more_style = '';
		break;	



	// Default is to include the main page.
	default:
		$page = 'home.inc.php';
		$page_title = 'Welcome';
		$style = 'style.css';
		break;
		
} // End of main switch.

// Make sure the file exists:
if (!file_exists('./modules/' . $page)) {
	$page = 'home.inc.php';
	$page_title = 'Site Home Page';
}

// Include the header file:
include_once ('inc/header.php');

// Include the content-specific module:
// $page is determined from the above switch.




include ('./modules/' . $page);

// Include the footer file to complete the template:
include_once ('inc/footer.php');

?>
