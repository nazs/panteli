<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("../assets/group.json");
$json = json_decode($jsondata, true);
$output = '<form action=index.php?p=group_breakfast method=post class="allForms"><table class="table table-condensed" >'."\n";
$output .= '<table class="table table-condensed">';
$count = 1;
foreach ($json['menu'] as $snack) {
	$no = $snack['no'];
	$output .= "<tr><td class='nos'>".$count."</td>";
	$output .= "<td><input type='text' name='item[$no]' value='". $snack['item']. "' class='item' ></td>";
	$output .= "<td class='price'><input type='text' name='price[$no]' value='". number_format($snack['price'],2). "' ></td></tr>";
	$count ++;

}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Group Breakfasts'></form>";
	echo $output;
?>	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


