<?php

if(isset($_POST['uid']) && isset($_POST['pwd'])) {
	$jsondata = file_get_contents("../../pan-users.json");
	$json = json_decode($jsondata);

	$pass = array();
	foreach($json->users as $user) {

		if($user->uid == $_POST['uid'] && $user->pwd == md5($_POST['pwd'])) {

			$_SESSION['admin'] = TRUE;
			$pass[] = $user->uid;
			$_SESSION['uid'] = $user->uid;

		} 
	}

	//echo sizeof($err);
	if(sizeof($pass) == 0 ) { 
		$_SESSION['error'] = '<p>Login failed.</p>'; 
		//echo $_SESSION['error']; 
	}

	header("Location: index.php?p=home");

} elseif(isset($_GET['now'])) {
	unset($_SESSION['admin']);
	header("Location: index.php");

} elseif(isset($_POST['newPass'])) {

	$new = $_POST['newPass'];
	$confirm = $_POST['confirmPass'];
	$uid = $_SESSION['uid'];

	if(empty($new) && empty($confirm)) {

		$_SESSION['password'] = 'Password confirm failed - please fill out both fields.';
		header("Location: index.php?p=home");

	} else {

		if($new == $confirm) {
			echo 'confirm ok';
	
			$jsondata = file_get_contents("../assets/users.json");
			$json = json_decode($jsondata);

		    foreach ($json->users as $user) {
		        
		        if($user->uid == $uid) {

		            $user->pwd = md5($new);
		        }
		    }

		    $newJsonString = json_encode($json);
		    file_put_contents('../assets/users.json', $newJsonString);        

		    header("Location: index.php?p=Restaurant");

		} else {

			$_SESSION['password'] = 'Password confirm failed - unmatched fields.';
			header("Location: index.php?p=home");
		}
	}
}

?>