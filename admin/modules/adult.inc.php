<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("../assets/adult.json");
$json = json_decode($jsondata, true);
$output = '<form action=index.php?p=adult method=post class="allForms"><table class="table table-condensed" >'."\n";
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/adult.php target=_blank>Print Menu</a></span></th></tr>';
$count = 1;
foreach ($json['menu'] as $snack) {
	$no = $snack['no'];
	$output .= "<tr>";
	$output .= "<td colspan=3><input type='text' name='item[$no]' value='". $snack['item'] ."' class='item' >";
	$output .= "<textarea name=desc[$no] rows=4 cols=100 class='item'>". $snack['description']. "</textarea></td>";
	$output .= "<td class='price'><input type='text' name='price[$no]' value='". number_format($snack['price'],2). "' ></td></tr>";
	$count ++;

}
	$output .= "<tr height='15px'><td colspan='3'></td></tr><tr><td></td><td colspan='2'><i>Please phone Canterbury (01227) 765506 to book. Fax also available on (01227) 765506.These are just a few suggestions. We are able to cater for your exact requirements. Please ask for a quotation.</i></td></tr>";
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Adult Groups'></form>";
	echo $output;


	$jsondata = file_get_contents("../assets/obj.json");
	$json = json_decode($jsondata);
	//foreach($json['menu'] as $menu) {
	//echo $json->menu->header;
	//var_dump($json);
	foreach($json->menu->items as $menu) {
		//echo $menu->id . '<br>';
		//echo $menu->id;

		//var_dump($menu);

	}
	//echo $json->menu->header;
	//var_dump($json);

?>	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>



<?php /*{
  "menu": {
    "header": "Main Menu",
    "items": [
        {"id": "Products"},
        {"id": "Support"},
        {"id": "About US"},
    ]
  }
}*/
?>