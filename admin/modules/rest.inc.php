<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("../assets/rest.json");
$json = json_decode($jsondata, true);
//var_dump($json);
$_SESSION['json'] = json_decode($jsondata);

$output = '<form action=index.php?p=restaurant method=post class="allForms"><table class="table table-condensed">'."\n"; // \r\n";
$output .= "\t".'<tr><th></th><th colspan="2">Snacks and Sandwiches<span class="pull-right"><a href=inc/self.php target=_blank>Print Menu</a></span></th></tr>'."\n";
$output .= "\t<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>"."\n";
$count = 1;

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}
array_sort_by_column($json['menu'], 'group');

//var_dump($json['menu']);

$old = '';
$nos = array();
foreach ($json['menu'] as $snack) {
	
	//if($snack['no'] <= 35) {
		$no = $snack['no']; $grp = $snack['group']; $inGrp = count($grp); 
		
		$price = number_format($snack['price'], 2);
		/*$link = "<a href='' data-toggle='modal' data-target='#add-item' id='Group $grp-$grp' class='trig-add-item'>Add Item</a>";*/
		$link = "<button data-toggle='modal' data-target='#add-item' id='Group $grp-$grp' class='trig-add-item btn btn-primary btn-xs pull-right'>Add Item</button>";
		if($old < $grp) { $output .= '<tr height=15><td colspan="2">'."Group $grp <span class='pull-right'>$link</span>".'</td></tr>';}

		$output .= "\t<tr><td class='nos'>".$count."</td>";
		$output .= '<td><input type="text" name="item['.$no.']" value="'. $snack['item']. '" class="item" ></td>';
		$output .= "<td class='price'><input type='text' value='". $price. "' name='price[$no]' >".
		'<input type="hidden" name="group['.$no.']" value="'.$grp.'" >'
		."</td></tr>\n";
		$old = $grp; // grab group # to compare to next one
		$old_no =$no;
		$nos[] = $no;

		$count ++;
	//}
}

	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Snacks and Sandwiches'></form>"."\n";
	echo $output;

	asort($nos); $_SESSION['no'] = end($nos) + 1;


	//echo $nos['no'];
?>	

		

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


