<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("../assets/snacks.json");
$json = json_decode($jsondata, true);

$_SESSION['json'] = json_decode($jsondata);

$output = '<form action=index.php?p=snacks method=post class="allForms"><table class="table table-condensed">'."\n"; // \r\n";
$output .= "\t".'<tr><th></th><th colspan="2">Snacks and Sandwiches<span class="pull-right"><a href=inc/self.php target=_blank>Print Menu</a></span></th></tr>'."\n";
$output .= "\t<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>"."\n";
$count = 1;

foreach ($json['menu'] as $snack) {
	//if($snack['no'] <= 35) {
		$no = $snack['no'];
		$price = number_format($snack['price'], 2);
		$output .= "\t<tr><td class='nos'>".$count.'</td><td><input type="text" name="item['.$no.']" value="'. $snack['item']. '" class="item" ></td>';
		$output .= "<td class='price'><input type='text' value='". $price. "' name='price[$no]' ></td></tr>"."\n";
		$count ++;
	//}
}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Snacks and Sandwiches'></form>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/child.json");
$json = json_decode($jsondata, true);
$output = '<a id=child></a><form action=index.php?p=child method=post><table class="table table-condensed" >'."\n";
$output .= "\t".'<tr><th></th><th colspan="2">Children'. "'s" .' Menu</th></tr>'."\n";
foreach ($json['menu'] as $child) {
	$price = number_format($child['price'],2);
	$no = $child['no'];
	$output .= "\t<tr><td class='nos'>".$count.'</td><td><input type="text" name="item['.$no.']" value="'. $child['item']. '" class="item" ></td>';
	$output .= "<td class='price'><input type=text value='". $price. "' name='price[$no]' ></td></tr>"."\n";
	$count ++;

}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value=\"Update Children's Menu\"></form>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/hot.json");
$json = json_decode($jsondata, true);
$output = '<a id=hot></a><form action=index.php?p=hotdrinks method=post><table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Hot Drinks</th><th class="price">Small</th><th class="price">Large</th></tr>'."\n";
foreach ($json['menu'] as $child) {
	$no = $child['no'];
	$small = number_format($child['price1'],2);
	$large = number_format($child['price2'],2);
	$output .= "\t<tr><td class='nos'>".$count.'</td><td><input type="text" name="item['.$no.']" value="'. $child['item']. '" class="item" ></td>';
	$output .= "<td class='price'><input type=text value='". $small. "' name='price[$no".'-price1'."]'></td>";
	$output .= "<td class='price'><input type=text value='". $large. "' name='price[$no".'-price2'."]'></td></tr>"."\n";
	$count ++;

}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Hot Drinks'></form>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/cold.json");
$json = json_decode($jsondata, true);

$output = '<a id=cold></a><form action=index.php?p=colddrinks method=post><table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Cold Drinks</th><th class="price">Regular</th><th class="price">Medium</th><th class="price">Large</th></tr>'."\n";
foreach ($json['menu'] as $child) {
	$no = $child['no'];
	$regular = number_format($child['price1'],2);
	$medium = number_format($child['price2'],2);
	$large = number_format($child['price3'],2);
	$item = $child['item'];
	$output .= "\t<tr><td class='nos'>".$count.'</td><td><input type="text" name="item['.$no.']" value="'. $child['item']. '" class="item" ></td>';
	$output .= "<td class='price'><input class=price2 type=text value='". $regular. "' name='price[$no".'-price1'."]'></td>";
	$output .= "<td class='price'><input type=text value='". $medium. "' name='price[$no".'-price2'."]' class=coldd></td>";
	$output .= "<td class='price'><input type=text value='". $large. "' name='price[$no".'-price3'."]'></td></tr>"."\n";
	$count ++;

}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Cold Drinks'></form>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/desserts.json");
$json = json_decode($jsondata, true);
$output = '<a id=dessert></a><form action=index.php?p=desserts method=post><table class="table table-condensed">'."\n";
$output .= "\t<tr><th></th><th>Desserts and Ice-Cream</th></tr>"."\n";
$output .= "\t<tr><td></td><td><i>All Ice Cream Dishes are made with Premium Ice Cream. See our Separate Ice Creams Menu</i></td></tr>"."\n";
foreach ($json['menu'] as $child) {
	$no = $child['no'];
	$price = number_format($child['price'],2);
	$output .= "\t<tr><td class='nos'>".$count."</td><td><input type='text' name='item[$no]' value='". $child['item']. "' class='item' ></td>";
	$output .= "<td class='price'><input type=text value='". $price. "' name='price[$no]' ></td></tr>"."\n";
	$count ++;

}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Dessserts and Ice-Cream'></form>"."\n";
	echo $output;	
?>		

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


