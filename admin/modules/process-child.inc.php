<?php

if(isset($_POST['price']))
{

    $jsondata = file_get_contents("../assets/child.json");
    $json = json_decode($jsondata);

	//var_dump($_POST);
    foreach ($_POST['price'] as $key => $value1) {

        foreach ($json->menu as $key2) {
            
            if($key2->no == $key) {
    
                $key2->price = (float) $value1;            
            }
        }
    }	
    
    foreach ($_POST['item'] as $key => $value1) {

        foreach ($json->menu as $key2) {
            
            if($key2->no == $key) {
    
                $key2->item = $value1;            
            }
        }
    }    

    $newJsonString = json_encode($json);
    file_put_contents('../assets/child.json', $newJsonString);        

    header("Location: index.php?p=Self-Service#child");

}

?>