<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("../assets/students.json");
$json = json_decode($jsondata, true);

$_SESSION['json'] = json_decode($jsondata);

$output = '<form action=index.php?p=students method=post class="allForms"><table class="table table-condensed">'."\n"; // \r\n";
$output .= "\t".'<tr><th></th><th colspan="2">Snacks and Sandwiches<span class="pull-right"><a href=inc/self.php target=_blank>Print Menu</a></span></th></tr>'."\n";
$output .= "\t<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>"."\n";
$count = 1;

foreach ($json['menu'] as $snack) {
	//if($snack['no'] <= 35) {
		$no = $snack['no'];
		$price = number_format($snack['price'], 2);
		$description = $snack['description'];
		$output .= "\t<tr><td class='nos'>".$count."</td>";
		$output .= "<td><input type='text' name='item[$no]' value='". $snack['item']. "' class='item' >";
		//$output .= "<div><input type='text' name='desc[$no]' value='".$description."' class='item' ></div></td>";
		$output .= "<div><textarea name='desc[$no]' >".$description."</textarea></div>";
		$output .= "<td class='price'><input type='text' value='". $price. "' name='price[$no]' ></td></tr>"."\n";
		$count ++;
	//}
}
	$output .= "</table><input class='btn btn-primary pull-right' type=submit value='Update Student Groups'></form>"."\n";
	echo $output;

?>		

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


