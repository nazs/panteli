<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
if(isset($_SESSION['password'])) {
	echo $_SESSION['password'];
	unset($_SESSION['password']);
}
?>


<div class="container">
	<div class="row">

		<main class="col-sm-8 col-sm-push-4 hometxt">
	
			<h2>Welcome to Admin</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, suscipit, harum quasi cupiditate aspernatur impedit cumque voluptatibus eum maxime soluta sit est at distinctio rerum itaque ipsum veritatis deleniti reiciendis.</p>
			<p>Possimus, eos commodi dolores dolor soluta ea molestias est explicabo ab placeat corporis id unde doloremque nesciunt maiores nihil necessitatibus? Ducimus quidem quam iure ad ut ea maiores sit ratione.</p>
			<p>Quia harum mollitia tempora perferendis repudiandae quis molestiae. Quo, harum, eveniet, laudantium dignissimos soluta id odio optio earum libero praesentium expedita odit sapiente asperiores voluptas deleniti. Recusandae qui debitis deserunt.</p>
			<p>Eligendi, similique, qui earum vitae natus esse deleniti vel nam ipsa dicta accusamus ad voluptates culpa in veritatis! Veniam, aut voluptate in iure corporis sunt sit minima accusantium voluptatem reprehenderit?</p>




	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php 
				if(isset($_SESSION['admin'])) 
				{
					include_once 'inc/nav.php'; 
				} else 
				{
					include 'inc/login.php';
				}

			?>
		</nav>		
	</div>
</div>


