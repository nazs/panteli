<div class="gen-pad">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		    <li data-target="#myCarousel" data-slide-to="3"></li>
		    <li data-target="#myCarousel" data-slide-to="4"></li>
		  </ol>	
		<section class="carousel-inner">
			<div class="active item"><img src="img/rest3.jpg" alt=""></div>
			<div class="item"><img src="img/rest2.jpg" alt=""></div>
			<div class="item"><img src="img/rest3.jpg" alt=""></div>
			<div class="item"><img src="img/rest4.jpg" alt=""></div>
			<div class="item"><img src="img/rest5.jpg" alt=""></div>
		</section>
		  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
	</div>
