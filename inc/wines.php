<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>

<body onload=window.print(); >
<link rel="stylesheet" href="../css/custom2.css">
<a class="print" href="javascript:window.print()">
<img src="../img/print.png">
</a>
<div>Wines and Beers</div> <center>FREE WIFI</center>

<?php
$jsondata = file_get_contents("../assets/ales.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">';
$output .= "<tr><td></td><td colspan='2'><i>We take great pleasure in stocking a selection of Ales from local breweries. Here we list some of our regulars. Due to the nature of these products we cannot always guarantee availability. Specials will be available throughout the year.</i></td></tr>";
$count = 1;
$old = '';
foreach ($json['menu'] as $snack) {
	$no = $snack['no']; $grp = $snack['group'];
	switch ($grp) {
		case '1':
			$name = 'RAMSGATE BREWERY';
			break;
		case '2':
			$name = 'WHITSTABLE BREWERY';
			break;
		case '3':
			$name = 'SHEPHERD NEAME';
			break;
		case '4':
			$name = 'PINE TREES FARM<div>DUDDA\'S TUN CIDER: KATY clean, crisp tasting farm-made cider using 100% apple juice grown at Pine Trees Farm</div>';
			break;
		case '5':
			$name = 'BARNSOLE VINEYARD';
			break;
		case '6':
			$name = 'OTHERS';
			break;
	}
	
	if($old < $grp) { $output .= '<tr ><td class="header pad" colspan="3">'.$name.'</td></tr>';}

	$output .= "<tr><td class='nos'>".$count."</td>";
	$output .= "<td>". $snack['item']. "</td>";
	$output .= "<td class='price'>&pound; ". number_format($snack['price'],2). "</td></tr>";
	$old = $grp;
	$count ++;


}
	$output .= "</table>";
	echo $output;
?>		
 <center>FREE WIFI</center>
</main>


