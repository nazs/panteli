<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php			
$jsondata = file_get_contents("assets/main.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">';
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/rest.php target=_blank>Print Menu</a></span></th></tr>';
//$output .= "<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>";
$count = 1;
foreach ($json['main'] as $snack) {
	if($snack['item'] == 'na') { 
		$output .= "\t".'<tr height="15px"><td colspan="2"><td></tr>'."\n";
	} else {

		$output .= "\t<tr><td class='nos'>".$count."</td>";
		$output .= "<td>". $snack['item']. "</td>";
		$output .= "<td class='price'>". $snack['price']. "</td></tr>\n";
		$count ++;
	}

}
	$output .= "</table>";
	echo $output;
?>
<!-- 			<table class="table table-hover table-condensed">
		<tr><td class="numbering">1</td><td>Soup of The Day</td><td colspan="3"  align="right" width="140px"  >&pound; 1.70</td></tr>
		<tr><td class="numbering">2</td><td>Roast Beef, Roast Pots, Yorkshire Pud, Veg</td><td colspan="3"  align="right" >&pound; 6.60</td></tr>
		<tr><td class="numbering">3</td><td>Roast Chicken, Roast Pots and Vegetables</td><td colspan="3"  align="right" >&pound; 6.60</td></tr>
		<tr><td class="numbering">4</td><td>Home Made Steak Pie, Boiled Pots & Veg</td><td colspan="3"  align="right" >&pound; 6.50</td></tr>
		<tr><td class="numbering">5</td><td>Home Made Steak Pie, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.50</td></tr>
		<tr><td class="numbering">6</td><td>Steak and Kidney Pudding, Boiled Pots,Veg</td><td colspan="3"  align="right" >&pound; 6.00</td></tr>
		<tr><td class="numbering">7</td><td>Fried Fillet of Cod, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.60</td></tr>
		<tr><td class="numbering">8</td><td>Fried Fillet of Plaice, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.80</td></tr>
		<tr><td class="numbering">9</td><td>Deep Fried Scampi, Chips & Peas</td><td colspan="3"  align="right" >&pound; 6.80</td></tr>
		<tr><td class="numbering">10</td><td>Grilled Rump Steak, Chips and Peas</td><td colspan="3"  align="right" >&pound; 11.20</td></tr>
		<tr><td class="numbering">11</td><td>Pork Chop, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.80</td></tr>
		<tr><td class="numbering">12</td><td>Lamb Chops, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.80</td></tr>
		<tr><td class="numbering">13</td><td>Grilled Gammon Steak, Chips and Peas</td><td colspan="3"  align="right" >&pound; 6.20</td></tr>
		<tr><td class="numbering">14</td><td>Hamburger Steak, Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.30</td></tr>
		<tr><td class="numbering">15</td><td>Quarter Pounder and Chips</td><td colspan="3"  align="right" >&pound; 4.30</td></tr>
		<tr><td class="numbering">16</td><td>Quarter Pounder</td><td colspan="3"  align="right" >&pound; 3.20</td></tr>
		<tr><td class="numbering">17</td><td>Egg Bacon Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.85</td></tr>
		<tr><td class="numbering">18</td><td>Egg Sausage Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.30</td></tr>
		<tr><td class="numbering">19</td><td>Double Egg and Chips and Peas</td><td colspan="3"  align="right" >&pound; 3.85</td></tr>
		<tr><td class="numbering">20</td><td>Ham Egg Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.85</td></tr>
		<tr><td class="numbering">21</td><td>Ham Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.20</td></tr>
		<tr><td class="numbering">22</td><td>Double Sausage Chips and Peas</td><td colspan="3"  align="right" >&pound; 4.30</td></tr>
		<tr><td class="numbering">23</td><td>Jacket Potato with Beans or Coleslaw</td><td colspan="3"  align="right" >&pound; 3.70</td></tr>
		<tr><td class="numbering">24</td><td>Jacket Potato with Cheese</td><td colspan="3"  align="right" >&pound; 3.70</td></tr>
		<tr><td class="numbering">25</td><td>Jacket Potato with Egg Mayonnaise</td><td colspan="3"  align="right" >&pound; 3.70</td></tr>
		<tr><td class="numbering">26</td><td>Jacket Potato with Tuna Fish Mayonnaise</td><td colspan="3"  align="right" >&pound; 4.20</td></tr>
		<tr><td class="numbering">27</td><td>Jacket Potato with Prawns</td><td colspan="3"  align="right" >&pound; 5.00</td></tr>
		<tr><td class="numbering">28</td><td>Chips Per Portion ? Small (gravy 10p extra)</td><td colspan="3"  align="right" >&pound; 1.40</td></tr>
		<tr><td class="numbering">29</td><td>Chips Per portion ? Large (gravy 10p extra)</td><td colspan="3"  align="right" >&pound; 1.99</td></tr>
		<tr><td class="numbering">30</td><td>Extra Vegetables per portion</td><td colspan="3"  align="right" >&pound; 0.90</td></tr>
		<tr><td class="numbering">31</td><td>Mushrooms Per Portion</td><td colspan="3"  align="right" >&pound; 1.30</td></tr>
		<tr><td class="numbering">32</td><td>Bread and Butter Per Slice</td><td colspan="3"  align="right" >&pound; 0.35</td></tr>
		<tr><td class="numbering">33</td><td>Ham, Cheese or Egg Salad</td><td colspan="3"  align="right" >&pound; 5.75</td></tr>
		<tr><td class="numbering">34</td><td>Roast Chicken Salad</td><td colspan="3"  align="right" >&pound; 6.60</td></tr>
		<tr><td class="numbering">35</td><td>Roast Beef Salad</td><td colspan="3"  align="right" >&pound; 6.60</td></tr>
		<tr><td class="numbering">36</td><td>Prawn Salad</td><td colspan="3"  align="right" >&pound; 7.20</td></tr>
	</table> -->	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


