<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
if(isset($_SESSION['submitted'])) {
	echo $_SESSION['submitted'];
	unset($_SESSION['submitted']);
}
?>


<div class="container">
	<div class="row">

		<main class="col-sm-8 col-sm-push-4 hometxt">
	
			<h2>Welcome to Panteli's of Canterbury</h2>
			<p>where we are open from Monday to Saturday, from 7am until 4pm, serving breakfast, lunch, afternoon tea and hot and cold snacks.</p>
			<p>Freshly made sandwiches, teas and coffees and hot meals are served all day at reasonable prices.</p>
			<p>Wines, beers, and spirits are available all day, with or without meals, throughout the premises.</p>
			<p>Where possible we source our produce locally and take pride in supporting small local producers. Examples of local produce include: free range eggs, ice-cream, beers, wines, potatoes, fresh fish, freshly baked bread, apple juice, cider, etc.</p>
			<p>Members of staff are trained to clear your table regularly. This is to keep the restaurant tidy and more comfortable for you. Please do not take it as a signal to leave. Please help us to keep the restaurant tidy by returning your empty tray to a member of the staff or to the counter.</p>
			<p>With our overall seating capacity of more than 160, we are able to cater for small and large parties and private functions at any time on any day (including Sundays). Please ask for a quotation.</p>
			<p>In our small and select licensed restaurant upstairs, we offer a quick, efficient waitress service from a comprehensive and reasonably priced menu.</p>




	
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


