<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';

?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php			
$jsondata = file_get_contents("assets/students.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">';
$output .= '<tr><th></th><th colspan="2"><span class="pull-right"><a href=inc/stud.php target=_blank>Print Menu</a></span></th></tr>';

$count = 1;
foreach ($json['students'] as $snack) {

	$output .= "<tr>";
	$output .= "<td width='70px'>". $snack['item']. "</td>";
	$output .= "<td>". $snack['description']. "</td>";
	$output .= "<td class='price'>". $snack['price']. "</td></tr>";
	//$count ++;

}
	$output .= "<tr height='15px'><td colspan='3'></td></tr><tr><td></td><td colspan='2'><i>Please phone Canterbury (01227) 765506 to book. Fax also available on (01227) 765506.These are just a few suggestions. We are able to cater for your exact requirements. Please ask for a quotation.</i></td></tr>";
	$output .= "</table>";
	echo $output;	
?>		
<!-- 			<table class="table table-hover table-condensed">
					<tr><td class="numbering"  valign="top" >1</td><td valign="top" ><strong>Menu 1</strong><div>
						Fruit Juice, Fillet of Cod, Chips and Tomato, Roll and Butter, Ice Cream</div></td><td valign="top"  colspan="3"  align="right" width="140px" >&pound; 8.25</td></tr>
					<tr><td class="numbering"  valign="top" >2</td><td valign="top" ><strong>Menu 2</strong><div>
						Fruit Juice, Beefburger, Chips and Tomato, Roll and Butter, Ice Cream</div></td><td valign="top"  colspan="3"  align="right" >&pound; 7.50</td></tr>
					<tr><td class="numbering"  valign="top" >3</td><td valign="top" ><strong>Menu 3</strong><div>
						Fruit Juice, Steak Pie and Vegetables, Roll and Butter, Ice Cream</div></td><td valign="top"  colspan="3"  align="right" >&pound; 8.50</td></tr>
					<tr><td class="numbering"  valign="top" >4</td><td valign="top" ><strong>Menu 4</strong><div>
						Fruit Juice, Ham, Egg, Chips and Tomato, Roll and Butter, Ice Cream</div></td><td valign="top"  colspan="3"  align="right" >&pound; 7.50</td></tr>
					<tr><td class="numbering"  valign="top" >5</td><td valign="top" ><strong>Menu 5</strong><div>
						Fruit Juice, Fried Fillet of Plaice, Chips and Tomato, Roll and Butter, Fruit Cocktail, Ice Cream</div></td><td valign="top"  colspan="3"  align="right" >&pound; 8.50</td></tr>
					<tr><td class="numbering"  valign="top" >6</td><td valign="top" ><strong>Menu 6</strong><div>
						Fruit Juice, Grilled Gammon Steak, Pineapple, Chips and Tomato Roll and Butter, Peach Melba</div></td><td valign="top"  colspan="3"  align="right" >&pound; 8.50</td></tr>
					<tr><td class="numbering"  valign="top" >7</td><td valign="top" ><strong>Menu 7</strong><div>
						Fruit Juice, Roast Beef, Yorkshire Pudding, Roast Potatoes and Vegetables, Roll and Butter, Apple pie and Custard</div></td><td valign="top"  colspan="3"  align="right" >&pound; 10.50</td></tr>
					<tr><td class="numbering"  valign="top" >8</td><td valign="top" ><strong>Menu 8</strong><div>
						Fruit Juice, Roast Chicken, Roast Potatoes, and Vegetables, Roll and Butter, Apple Pie and Custard</div></td><td valign="top"  colspan="3"  align="right" >&pound; 10.50</td></tr>
					<tr><td class="numbering"  valign="top" ></td><td valign="top" ><strong>Please phone Canterbury (01227) 765506 to book. Fax also available on (01227) 765506.<br />These are</strong><div>
						</div></td><td valign="top"  colspan="3"  align="right" ></td></tr>
					<tr><td></td><td colspan="5"><i>Please phone Canterbury (01227) 765506 to book. Fax also available on (01227) 
								765506.<br />
								These are just a few suggestions. We are able to cater for your exact requirements. Please ask for a 
								quotation.</i></td></tr>
				</table> -->				
		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


