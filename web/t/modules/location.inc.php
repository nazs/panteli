?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;
?>

<section class="container">
	<div class="row">
		<main class="col-lg-9 col-md-9 col-sm-9 col-lg-push-3 col-md-push-3 col-sm-push-3 col-xs-push-3 clearfix"><img src="img/map.jpg" alt="" class="img-responsive img-rounded">
		

		</main>
		<?php include_once 'inc/aside.php'; ?>			
	</div>
</section> <!-- container -->