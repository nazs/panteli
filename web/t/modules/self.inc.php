<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">
<?php
$jsondata = file_get_contents("assets/snacks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n"; // \r\n";
$output .= "\t".'<tr><th></th><th colspan="2">Snacks and Sandwiches<span class="pull-right"><a href=inc/self.php target=_blank>Print Menu</a></span></th></tr>'."\n";
$output .= "\t<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>"."\n";
$count = 1;
foreach ($json['snacks'] as $snack) {

	$output .= "\t<tr><td class='nos'>".$count."</td>";
	$output .= "<td>". $snack['item']. "</td>";
	$output .= "<td class='price'>". $snack['price']. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("assets/childrens menu.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed" >'."\n";
$output .= "\t".'<tr><th></th><th colspan="2">Children'. "'s" .' Menu</th></tr>'."\n";
foreach ($json['children'] as $child) {

	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $child['price']. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("assets/hot drinks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Hot Drinks</th><th class="price">Small</th><th class="price">Large</th></tr>'."\n";
foreach ($json['hot'] as $child) {

	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $child['Small']. "</td>";
	$output .= "<td class='price'>". $child['Large']. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("assets/cold drinks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Cold Drinks</th><th class="price">Regular</th><th class="price">Medium</th><th class="price">Large</th></tr>'."\n";
foreach ($json['cold'] as $child) {

	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $child['Regular']. "</td>";
	$output .= "<td class='price'>". $child['Medium']. "</td>";
	$output .= "<td class='price'>". $child['Large']. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("assets/desserts.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t<tr><th></th><th>Desserts and Ice</th></tr>"."\n";
$output .= "\t<tr><td></td><td><i>All Ice Cream Dishes are made with Premium Ice Cream. See our Separate Ice Creams Menu</i></td></tr>"."\n";
foreach ($json['desserts'] as $child) {

	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $child['price']. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;	
?>		

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


