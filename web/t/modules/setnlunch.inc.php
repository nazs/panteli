<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;
?>

<section class="container">
	<div class="row">
		<main class="col-lg-9 col-md-9 col-sm-9">
<table width=100% cellspacing="0" border="0" class="table table-condensed table-hover">

				<thead><tr><th>&nbsp;</th><th colspan="5">Lunch Menus</th><th colspan="2">1-Course</th><th colspan="3">2-Course</th></tr></thead>
				<tbody><tr><td class="numbering">1</td><td>Red curry  - duck, chicken, pork or beef served with rice.</td><td align="right"  width="140px" colspan="6">£ 4.95</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">2</td><td>Green curry - chicken, pork or beef served with rice.</td><td align="right" colspan="6">£ 4.95</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">3</td><td>Stir fried pork - beef or chicken with chilli and basil leaves with rice.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">4</td><td>Stir fried pork - beef or chicken with chilli and basil leaves with rice with fried egg.</td><td align="right" colspan="6">£ 5.00</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">5</td><td>Fried pad thai noodles with chicken.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">6</td><td>Fried noodles with pork, beef or chicken and mixed vegetables in soy gravy sauce</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">7</td><td>Fried noodles with seafood and mixed vegetables in soy gravy sauce.</td><td align="right" colspan="6">£ 4.95</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">8</td><td>Fried rice with pork or chicken.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">9</td><td>Fried rice with prawns. </td><td align="right" colspan="6">£ 4.95</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">10</td><td>Stir fried pork, or chicken with garlic and pepper served with rice.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">11</td><td>Stir fried pork, beef or chicken with fresh chilli and served with rice.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">12</td><td>With fried egg.</td><td align="right" colspan="6">£ 5.00</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td class="numbering">13</td><td>Fried mixed vegetables with chicken, pork or beef with rice.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">14</td><td>Fried bean curd and cashew nuts with mixed vegetables served with rice.</td><td align="right" colspan="6">£ 4.50</td>
				<td align="right" colspan="3">£ 6.95</td></tr><tr><td class="numbering">15</td><td>Sweet and sour prawns served with rice.</td><td align="right" colspan="6">£ 4.95</td>
				<td align="right" colspan="3">£ 7.45</td></tr><tr><td colspan="9">&nbsp;</td></tr>
				<tr><td>&nbsp;</td><td colspan="5"><strong>Choose any one of the following starter dishes as a 2-course lunch</strong></td></tr><td colspan="3">&nbsp;</td></tr><tr><td class="numbering">16</td><td>Chicken satay  ( 4 pieces)</td><td align="right" colspan="9"></td></tr><tr><td class="numbering">17</td><td>Spring rolls   (5 pieces)</td><td align="right" colspan="9"></td></tr><tr><td class="numbering">18</td><td>Tom Yum Gai   (Hot &amp; Sour Chicken Soup)</td><td align="right" colspan="9"></td></tr><tr><td class="numbering">19</td><td>Steamed dumpling  (4 Pieces)</td><td align="right" colspan="9"></td></tr><tr><td class="numbering">20</td><td>Thai Fish Cakes  (3 pieces)</td><td align="right" colspan="9"></td></tr><tr><td colspan="9">&nbsp;</td></tr>
				<tr><td>&nbsp;</td><td colspan="5"><strong>Set Menus</strong></td><th holspan="3">&nbsp;</td></tr><tr><td class=hnumbering">21</td><td>Menu A (Minimum for 2 persons)</td><td align="right" colspan="9">£ 18.95</td></tr><tr><td class="numbering">22</td><td>Menu B (Minimum for 2 person)</td><td align="right" colspan="9">£ 18.95</td></tr><tr><td class="numbering">23</td><td>Menu C (Minimum for 4 person)</td><td align="right" colspan="9">£ 18.95</td></tr><tr><td class="numbering">24</td><td>Menu D (Minimum for 4 persons)</td><td align="right" colspan="9">£ 18.95</td></tr><tr><td class="numbering">25</td><td>Menu E (Minimum for 3 persons)</td><td align="right" colspan="9">£ 18.95</td></tr></tbody></table>
		</main>
		<?php include_once 'inc/aside.php'; ?>
	</div>
</section> <!-- container -->