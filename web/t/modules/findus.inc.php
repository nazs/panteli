<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../inc/config.inc.php');
	
	// Redirect to the index page:
	$url = BASE_URL . 'index.php';
	header ("Location: $url");
	exit;
	
} // End of defined() IF.
	//require_once DB;

//include_once 'inc/carousel.php';
?>


<div class="container menus"><span class="menus visible-xs"><?php echo $page_title;?></span>
	<div class="row">
		<main class="col-sm-8 col-sm-push-4">

<iframe width="100%" scrolling="no" height="500px" frameborder="0" src="http://maps.google.co.uk/maps?q=5+canterbury+lane+ct1+2hl&amp;ie=UTF8&amp;hq=&amp;hnear=5+Canterbury+Ln,+Canterbury,+Kent+CT1,+United+Kingdom&amp;ll=51.278192,1.083467&amp;spn=0.010792,0.014634&amp;t=m&amp;z=14&amp;output=embed" marginwidth="0" marginheight="0"></iframe>

		</main>		
		<nav class="col-sm-4 col-sm-pull-8">
			<?php include_once 'inc/nav.php'; ?>
		</nav>		
	</div>
</div>


