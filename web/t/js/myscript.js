$(function() {

	//make menus drop automatically
	$('ul.nav li.dropdown').hover(function() {
		$('.dropdown-menu', this).fadeIn();
	}, function() {
		$('.dropdown-menu', this).fadeOut('fast');
	});

	// show modals
	$('.modalphotos img').on('click', function() {
		$('#modal').modal({
			show: true,
		})

		var mysrc = this.src.substr(0, this.src.length-7) + '.jpg';
		$('#modalimage').attr('src', mysrc);
		var imgtag = this.alt;
		$('#imagetag').html(imgtag);
		$('#modalimage').on('click', function(){
			$('#modal').modal('hide')
		})
	});

	//$('#contact-email').val('hello');
	var email = $('#contact-email');
	var name = $('#contact-name');
	var msg = $('#contact-message');
	var sDate = $('#start2');
	var eDate = $('#end2');
	var errCount = []; //['hello', 'world', 'me'];
	
	
 	$("#contact-form").submit( function() {
 	//$("input[type=submit]").click( function() {
		//$(email).css(loginErr2).val('');

		var errCount = [];
		if(email.val() == '') {
			$('#emailReq').html('email required');
			errCount.push('email');
			//return false;
		}
		if(name.val() == '') {
			$('#nameReq').html('name required');
			errCount.push('name');
			//return false;
		}
		if(msg.val() == '') {
			$('#msgReq').html('message required');
			errCount.push('msg');
			//return false;
		}

		if(sDate.val() == '') {
			$('#startReq').html('Start Date required');
			errCount.push('start');
		}
		if(eDate.val() == '') {
			$('#endReq').html('End Date required');
			errCount.push('end');
		}

		if(errCount.length > 0) {
			return false;
			//$('#counter').html(errCount.length);
		}		
	});
	
	$('#contact-email').focus(function() {
		errCount.pop('email');
		$('#emailReq').html('');
	});	

	$('#contact-name').focus(function() {
		errCount.pop('name');
		$('#nameReq').html('');
	});

	$('#contact-message').focus(function() {
		errCount.pop('msg');
		$('#msgReq').html('');
	});

	$('#start2').focus(function() {
		errCount.pop('start');
		$('#startReq').html('');
	})
	$('#end2').focus(function() {
		errCount.pop('end');
		$('#endReq').html('');
	})

});

