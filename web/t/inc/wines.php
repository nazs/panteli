<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>

<body onload=window.print(); >
<link rel="stylesheet" href="../css/custom2.css">
<a class="print" href="javascript:window.print()">
<img src="../img/print.png">
</a>
<div>Wines and Beers</div>

<?php
$jsondata = file_get_contents("../assets/ales.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">';
$output .= "<tr><td></td><td colspan='2'><i>We take great pleasure in stocking a selection of Ales from local breweries. Here we list some of our regulars. Due to the nature of these products we cannot always guarantee availability. Specials will be available throughout the year.</i></td></tr>";
$count = 1;
foreach ($json['ales'] as $snack) {
	$item = str_replace('£', '&pound;', $snack['price']); 
	$header = strpos($snack['item'], 'header');
	if($snack['item'] == 'na') { 
		$output .= '<tr height="15px"><td colspan="2"><td></tr>';
	} elseif ($header !== false) {
		$item = str_replace('header ', '', $snack['item']);
		$output .= "<tr><td></td><td>".$item."</td></tr>";
	} else {
		$output .= "<tr><td class='nos'>".$count."</td>";
		$output .= "<td>". $snack['item']. "</td>";
		$output .= "<td class='price'>". $item. "</td></tr>";
		$count ++;
	}

}
	$output .= "</table>";
	echo $output;
?>		
</main>


