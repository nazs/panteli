<?php $_SESSION['contact'] = TRUE;?>
<div class="container">
	<!-- MODAL CONTACT FORM -->
	<div class="modal " id="modal-contact-form">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times;</button>
					<h3>Contact Panteli's of CANTERBURY</h3>
				</div>
				<div class="modal-body">
					<form action="index.php?p=process-contact" role="form" id="contact-form" method="post">
						<div class="form-group">
							<label for="contact-name">Name:</label><span id="nameReq" class="frm-err pull-right"></span>
							<div class="controls">
								<input type="text" id="contact-name" name="contact-name" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="contact-email">Email:</label><span id="emailReq" class="frm-err pull-right"></span>
							<div class="controls">
								<input type="email" id="contact-email" name="contact-email" class="form-control">
							</div>
						</div>	
									
						<div class="form-group">
							<label for="contact-message">Message:</label><span id="msgReq" class="frm-err pull-right"></span>
							<div class="controls">
								<textarea name="contact-message" class="form-control" id="contact-message" cols="30" rows="10" wrap="hard"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="controls">
								<input type="submit" class="btn btn-primary" value="Send Message"> <span id="counter"></span>
								 <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
							</div>
						</div>
					</form>
				</div>
			</div> <!-- modal-content -->
		</div> <!-- modal-dialog -->
	</div> <!-- end modal -->
</div>



