<?php # Script 2.5 - main.inc.php

/* 
 *	This is the main content module.
 *	This page is included by index.php.
 */

	//require_once DB;

//include_once 'inc/carousel.php';



	//echo file_get_contents("assets/snacks.json");

?>
<body onload=window.print(); >
<link rel="stylesheet" href="../css/custom2.css">
<a class="print" href="javascript:window.print()">
<img src="../img/print.png">
</a>
<div>Self Service Menu</div>

<?php
$jsondata = file_get_contents("../assets/snacks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n"; // \r\n";
$output .= "\t".'<tr><th></th><th colspan="2">Snacks and Sandwiches</th></tr>'."\n";
$output .= "\t<tr><td></td><td colspan='2'><i>Afternoon Teas, Snacks, Sandwiches and Filled Rolls Served All Day</i></td></tr>"."\n";
$count = 1;
foreach ($json['snacks'] as $snack) {
	$item = str_replace('£', '&pound;', $snack['price']); 
	$output .= "\t<tr><td class='nos'>".$count."</td>";
	$output .= "<td>". $snack['item']. "</td>";
	$output .= "<td class='price'>". $item. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/childrens menu.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed" >'."\n";
$output .= "\t".'<tr><th></th><th colspan="2">Children'. "'s" .' Menu</th></tr>'."\n";
foreach ($json['children'] as $child) {
	$item = str_replace('£', '&pound;', $child['price']); 
	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $item. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/hot drinks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Hot Drinks</th><th class="price">Small</th><th class="price">Large</th></tr>'."\n";
foreach ($json['hot'] as $child) {
	$item = str_replace('£', '&pound;', $child['Small']); 
	$item2 = str_replace('£', '&pound;', $child['Large']); 
	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $item. "</td>";
	$output .= "<td class='price'>". $item2. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/cold drinks.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t".'<tr><th></th><th>Cold Drinks</th><th class="price">Regular</th><th class="price">Medium</th><th class="price">Large</th></tr>'."\n";
foreach ($json['cold'] as $child) {
	$item = str_replace('£', '&pound;', $child['Regular']); 
	$item2 = str_replace('£', '&pound;', $child['Medium']); 
	$item3 = str_replace('£', '&pound;', $child['Large']); 

	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $item. "</td>";
	$output .= "<td class='price'>". $item2. "</td>";
	$output .= "<td class='price'>". $item3. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;

$jsondata = file_get_contents("../assets/desserts.json");
$json = json_decode($jsondata, true);
$output = '<table class="table table-condensed">'."\n";
$output .= "\t<tr><th></th><th>Desserts and Ice</th></tr>"."\n";
$output .= "\t<tr><td></td><td><i>All Ice Cream Dishes are made with Premium Ice Cream. See our Separate Ice Creams Menu</i></td></tr>"."\n";
foreach ($json['desserts'] as $child) {
	$item = str_replace('£', '&pound;', $child['price']); 
	$output .= "\t<tr><td class='nos'>".$count."</td><td>". $child['item']. "</td>";
	$output .= "<td class='price'>". $item. "</td></tr>"."\n";
	$count ++;

}
	$output .= "</table>"."\n";
	echo $output;	
?>		
</main>


